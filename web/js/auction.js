"use strict";
var firebaseConfig = {
    apiKey: "AIzaSyCWLS82pyCLc4FjaylsO5Npvtj5-hp-ld0",
    authDomain: "artinlog-crawler.firebaseapp.com",
    databaseURL: "https://artinlog-crawler.firebaseio.com",
    projectId: "artinlog-crawler",
    storageBucket: "artinlog-crawler.appspot.com",
    messagingSenderId: "389509051640",
    appId: "1:389509051640:web:bb853c6175351d42"
};
firebase.initializeApp(firebaseConfig);
var afterAjaxAuction = {
    init: function () {
        !function () {
            var t = $(".auction__sliderBig").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: !0,
                fade: !0,
                prevArrow: '<div class="slick-prev"></div>',
                nextArrow: '<div class="slick-next"></div>'
            });
            $(".auction__sliderThumb").slick({
                slidesPerRow: 5,
                rows: 2,
                slidesToScroll: 1,
                focusOnSelect: !0,
                centerPadding: "0px",
                swipeToSlide: !0
            }), $(".auction__sliderBig").on("afterChange", function (e, a, t, i) {
                var s = $(".auction__sliderThumb");
                s.find(".slick-active").removeClass("slick-active"), s.find('.auction__sliderThumb-slide[data-slide="' + parseInt(t + 1) + '"]').addClass("slick-active")
            }), $(document).on("click", ".auction__sliderThumb-slide", function (e) {
                e.preventDefault();
                var a = $(this).data("slide");
                t.slick("slickGoTo", parseInt(a - 1), !1)
            })
        }(), function () {
            function e() {
                var e = $(".columnResizeGallery").outerHeight(!0);
                $(".columnResizeTable").height(e - 10)
            }

            setTimeout(e, 300), $(window).on("resize", function () {
                e()
            })
        }()
    }
};
jQuery(document).ready(function () {
    $(document).on("submit", ".ajaxFormAuction", function (e) {
        e.preventDefault();
        var a = $(this), t = a.attr("action"), i = a.data("text-preload"), s = a.data("type"), n = a.data("page"),
            o = a.data("result"), c = {};
        if (a.find('[type="submit"]').prop("disabled", !0), $(o).html(""), $(".auction__search-progress").addClass("active"), $(o).html('<div class="auction__result-preload">' + i + "</div>"), $.ajax({
            type: "GET",
            url: t,
            data: a.serialize(),
            dataType: "json",
            crossDomain: !0,
            success: function (e) {
                var a = null;
                "auction" === s ? a = "/auction_responses/" : "tracking" === s && (a = "/track_responses/"), firebase.database().ref(a + e.data).on("value", function (e) {
                    c = e.val()
                })
            },
            error: function (e, a) {
                console.log(a)
            }
        }), $(".auctionAjaxResult").length) var r = $(".auctionAjaxResult").offset().top - $(".header-container").outerHeight() - 60;
        setTimeout(function () {
            var e = {action: "carbuy_auction_firebase", data: c, type: s, page_id: n, form: a.serialize()};
            $.post(ajax_auction_object.ajaxurl, e, function (e) {
            }).done(function (e) {
                $(o).html(e), "auction" === s && afterAjaxAuction.init()
            }), $("body,html").animate({scrollTop: r}, 400, function () {
                $(".auction__search-progress.active").removeClass("active"), $(a).trigger("reset"), $(a).find('[type="submit"]').prop("disabled", !1), "tracking" === s && $(".auction__header, .auction__search").hide()
            })
        }, 4e3)
    }), $(document).on("click", ".columnResizeTableOpen", function (e) {
        e.preventDefault();
        var a = $(this), t = a.data("text-open"), i = a.data("text-close"), s = $(".columnResizeTable");
        s.hasClass("active") ? (s.removeClass("active"), a.find("span").text(t)) : (s.addClass("active"), a.find("span").text(i))
    }), $(document).on("click", '.auction__tabs-list [data-toggle="tab"]', function (e) {
        e.preventDefault();
        var a = $(this).attr("href");
        return $(".auction__tabs-list").find(".active").removeClass("active"), $(this).parent().addClass("active"), $(".auction__tabs-content").find('[role="tabpanel"].active').removeClass("active"), $(a).addClass("active"), !1
    })
});