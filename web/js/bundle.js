"use strict";

function _typeof(e) {
    return (_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e
    } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
    })(e)
}

!function (e, t) {
    "object" == ("undefined" == typeof module ? "undefined" : _typeof(module)) && "object" == _typeof(module.exports) ? module.exports = e.document ? t(e, !0) : function (e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : void 0, function (k, e) {
    var t = [], S = k.document, i = Object.getPrototypeOf, a = t.slice, m = t.concat, l = t.push, o = t.indexOf, n = {},
        r = n.toString, h = n.hasOwnProperty, s = h.toString, c = s.call(Object), v = {};

    function g(e, t) {
        var n = (t = t || S).createElement("script");
        n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
    }

    function u(e, t) {
        return t.toUpperCase()
    }

    var d = "3.2.1", C = function e(t, n) {
        return new e.fn.init(t, n)
    }, p = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, f = /^-ms-/, y = /-([a-z])/g;

    function b(e) {
        var t = !!e && "length" in e && e.length, n = C.type(e);
        return "function" !== n && !C.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
    }

    C.fn = C.prototype = {
        jquery: d, constructor: C, length: 0, toArray: function () {
            return a.call(this)
        }, get: function (e) {
            return null == e ? a.call(this) : e < 0 ? this[e + this.length] : this[e]
        }, pushStack: function (e) {
            var t = C.merge(this.constructor(), e);
            return t.prevObject = this, t
        }, each: function (e) {
            return C.each(this, e)
        }, map: function (n) {
            return this.pushStack(C.map(this, function (e, t) {
                return n.call(e, t, e)
            }))
        }, slice: function () {
            return this.pushStack(a.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (e) {
            var t = this.length, n = +e + (e < 0 ? t : 0);
            return this.pushStack(0 <= n && n < t ? [this[n]] : [])
        }, end: function () {
            return this.prevObject || this.constructor()
        }, push: l, sort: t.sort, splice: t.splice
    }, C.extend = C.fn.extend = function () {
        var e, t, n, i, o, r, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == _typeof(s) || C.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++) if (null != (e = arguments[a])) for (t in e) n = s[t], s !== (i = e[t]) && (c && i && (C.isPlainObject(i) || (o = Array.isArray(i))) ? (r = o ? (o = !1, n && Array.isArray(n) ? n : []) : n && C.isPlainObject(n) ? n : {}, s[t] = C.extend(c, r, i)) : void 0 !== i && (s[t] = i));
        return s
    }, C.extend({
        expando: "jQuery" + (d + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
            throw new Error(e)
        }, noop: function () {
        }, isFunction: function (e) {
            return "function" === C.type(e)
        }, isWindow: function (e) {
            return null != e && e === e.window
        }, isNumeric: function (e) {
            var t = C.type(e);
            return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
        }, isPlainObject: function (e) {
            var t, n;
            return !(!e || "[object Object]" !== r.call(e) || (t = i(e)) && ("function" != typeof (n = h.call(t, "constructor") && t.constructor) || s.call(n) !== c))
        }, isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        }, type: function (e) {
            return null == e ? e + "" : "object" == _typeof(e) || "function" == typeof e ? n[r.call(e)] || "object" : _typeof(e)
        }, globalEval: function (e) {
            g(e)
        }, camelCase: function (e) {
            return e.replace(f, "ms-").replace(y, u)
        }, each: function (e, t) {
            var n, i = 0;
            if (b(e)) for (n = e.length; i < n && !1 !== t.call(e[i], i, e[i]); i++) ; else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
            return e
        }, trim: function (e) {
            return null == e ? "" : (e + "").replace(p, "")
        }, makeArray: function (e, t) {
            var n = t || [];
            return null != e && (b(Object(e)) ? C.merge(n, "string" == typeof e ? [e] : e) : l.call(n, e)), n
        }, inArray: function (e, t, n) {
            return null == t ? -1 : o.call(t, e, n)
        }, merge: function (e, t) {
            for (var n = +t.length, i = 0, o = e.length; i < n; i++) e[o++] = t[i];
            return e.length = o, e
        }, grep: function (e, t, n) {
            for (var i = [], o = 0, r = e.length, s = !n; o < r; o++) !t(e[o], o) != s && i.push(e[o]);
            return i
        }, map: function (e, t, n) {
            var i, o, r = 0, s = [];
            if (b(e)) for (i = e.length; r < i; r++) null != (o = t(e[r], r, n)) && s.push(o); else for (r in e) null != (o = t(e[r], r, n)) && s.push(o);
            return m.apply([], s)
        }, guid: 1, proxy: function (e, t) {
            var n, i, o;
            if ("string" == typeof t && (n = e[t], t = e, e = n), C.isFunction(e)) return i = a.call(arguments, 2), (o = function () {
                return e.apply(t || this, i.concat(a.call(arguments)))
            }).guid = e.guid = e.guid || C.guid++, o
        }, now: Date.now, support: v
    }), "function" == typeof Symbol && (C.fn[Symbol.iterator] = t[Symbol.iterator]), C.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
        n["[object " + t + "]"] = t.toLowerCase()
    });
    var w = function (n) {
        function d(e, t, n) {
            var i = "0x" + t - 65536;
            return i != i || n ? t : i < 0 ? String.fromCharCode(65536 + i) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
        }

        function o() {
            T()
        }

        var e, f, w, r, s, h, p, m, x, l, c, T, k, a, S, v, u, g, y, C = "sizzle" + 1 * new Date, b = n.document, A = 0,
            i = 0, $ = se(), E = se(), j = se(), L = function (e, t) {
                return e === t && (c = !0), 0
            }, D = {}.hasOwnProperty, t = [], N = t.pop, O = t.push, q = t.push, H = t.slice, M = function (e, t) {
                for (var n = 0, i = e.length; n < i; n++) if (e[n] === t) return n;
                return -1
            },
            P = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            I = "[\\x20\\t\\r\\n\\f]", F = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            z = "\\[" + I + "*(" + F + ")(?:" + I + "*([*^$|!~]?=)" + I + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + F + "))|)" + I + "*\\]",
            R = ":(" + F + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + z + ")*)|.*)\\)|)",
            W = new RegExp(I + "+", "g"), _ = new RegExp("^" + I + "+|((?:^|[^\\\\])(?:\\\\.)*)" + I + "+$", "g"),
            B = new RegExp("^" + I + "*," + I + "*"), X = new RegExp("^" + I + "*([>+~]|" + I + ")" + I + "*"),
            U = new RegExp("=" + I + "*([^\\]'\"]*?)" + I + "*\\]", "g"), Y = new RegExp(R),
            V = new RegExp("^" + F + "$"), G = {
                ID: new RegExp("^#(" + F + ")"),
                CLASS: new RegExp("^\\.(" + F + ")"),
                TAG: new RegExp("^(" + F + "|[*])"),
                ATTR: new RegExp("^" + z),
                PSEUDO: new RegExp("^" + R),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + I + "*(even|odd|(([+-]|)(\\d*)n|)" + I + "*(?:([+-]|)" + I + "*(\\d+)|))" + I + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + P + ")$", "i"),
                needsContext: new RegExp("^" + I + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + I + "*((?:-\\d)?\\d*)" + I + "*\\)|)(?=[^-]|$)", "i")
            }, K = /^(?:input|select|textarea|button)$/i, Q = /^h\d$/i, Z = /^[^{]+\{\s*\[native \w/,
            J = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ee = /[+~]/,
            te = new RegExp("\\\\([\\da-f]{1,6}" + I + "?|(" + I + ")|.)", "ig"),
            ne = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, ie = function (e, t) {
                return t ? "\0" === e ? "пїЅ" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            }, oe = ye(function (e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }, {dir: "parentNode", next: "legend"});
        try {
            q.apply(t = H.call(b.childNodes), b.childNodes), t[b.childNodes.length].nodeType
        } catch (e) {
            q = {
                apply: t.length ? function (e, t) {
                    O.apply(e, H.call(t))
                } : function (e, t) {
                    for (var n = e.length, i = 0; e[n++] = t[i++];) ;
                    e.length = n - 1
                }
            }
        }

        function re(e, t, n, i) {
            var o, r, s, a, l, c, u, d = t && t.ownerDocument, p = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== p && 9 !== p && 11 !== p) return n;
            if (!i && ((t ? t.ownerDocument || t : b) !== k && T(t), t = t || k, S)) {
                if (11 !== p && (l = J.exec(e))) if (o = l[1]) {
                    if (9 === p) {
                        if (!(s = t.getElementById(o))) return n;
                        if (s.id === o) return n.push(s), n
                    } else if (d && (s = d.getElementById(o)) && y(t, s) && s.id === o) return n.push(s), n
                } else {
                    if (l[2]) return q.apply(n, t.getElementsByTagName(e)), n;
                    if ((o = l[3]) && f.getElementsByClassName && t.getElementsByClassName) return q.apply(n, t.getElementsByClassName(o)), n
                }
                if (f.qsa && !j[e + " "] && (!v || !v.test(e))) {
                    if (1 !== p) d = t, u = e; else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((a = t.getAttribute("id")) ? a = a.replace(ne, ie) : t.setAttribute("id", a = C), r = (c = h(e)).length; r--;) c[r] = "#" + a + " " + ge(c[r]);
                        u = c.join(","), d = ee.test(e) && me(t.parentNode) || t
                    }
                    if (u) try {
                        return q.apply(n, d.querySelectorAll(u)), n
                    } catch (e) {
                    } finally {
                        a === C && t.removeAttribute("id")
                    }
                }
            }
            return m(e.replace(_, "$1"), t, n, i)
        }

        function se() {
            var i = [];
            return function e(t, n) {
                return i.push(t + " ") > w.cacheLength && delete e[i.shift()], e[t + " "] = n
            }
        }

        function ae(e) {
            return e[C] = !0, e
        }

        function le(e) {
            var t = k.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function ce(e, t) {
            for (var n = e.split("|"), i = n.length; i--;) w.attrHandle[n[i]] = t
        }

        function ue(e, t) {
            var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (i) return i;
            if (n) for (; n = n.nextSibling;) if (n === t) return -1;
            return e ? 1 : -1
        }

        function de(t) {
            return function (e) {
                return "input" === e.nodeName.toLowerCase() && e.type === t
            }
        }

        function pe(n) {
            return function (e) {
                var t = e.nodeName.toLowerCase();
                return ("input" === t || "button" === t) && e.type === n
            }
        }

        function fe(t) {
            return function (e) {
                return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && oe(e) === t : e.disabled === t : "label" in e && e.disabled === t
            }
        }

        function he(s) {
            return ae(function (r) {
                return r = +r, ae(function (e, t) {
                    for (var n, i = s([], e.length, r), o = i.length; o--;) e[n = i[o]] && (e[n] = !(t[n] = e[n]))
                })
            })
        }

        function me(e) {
            return e && void 0 !== e.getElementsByTagName && e
        }

        for (e in f = re.support = {}, s = re.isXML = function (e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, T = re.setDocument = function (e) {
            var t, n, i = e ? e.ownerDocument || e : b;
            return i !== k && 9 === i.nodeType && i.documentElement && (a = (k = i).documentElement, S = !s(k), b !== k && (n = k.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", o, !1) : n.attachEvent && n.attachEvent("onunload", o)), f.attributes = le(function (e) {
                return e.className = "i", !e.getAttribute("className")
            }), f.getElementsByTagName = le(function (e) {
                return e.appendChild(k.createComment("")), !e.getElementsByTagName("*").length
            }), f.getElementsByClassName = Z.test(k.getElementsByClassName), f.getById = le(function (e) {
                return a.appendChild(e).id = C, !k.getElementsByName || !k.getElementsByName(C).length
            }), f.getById ? (w.filter.ID = function (e) {
                var t = e.replace(te, d);
                return function (e) {
                    return e.getAttribute("id") === t
                }
            }, w.find.ID = function (e, t) {
                if (void 0 !== t.getElementById && S) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }) : (w.filter.ID = function (e) {
                var n = e.replace(te, d);
                return function (e) {
                    var t = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                    return t && t.value === n
                }
            }, w.find.ID = function (e, t) {
                if (void 0 !== t.getElementById && S) {
                    var n, i, o, r = t.getElementById(e);
                    if (r) {
                        if ((n = r.getAttributeNode("id")) && n.value === e) return [r];
                        for (o = t.getElementsByName(e), i = 0; r = o[i++];) if ((n = r.getAttributeNode("id")) && n.value === e) return [r]
                    }
                    return []
                }
            }), w.find.TAG = f.getElementsByTagName ? function (e, t) {
                return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : f.qsa ? t.querySelectorAll(e) : void 0
            } : function (e, t) {
                var n, i = [], o = 0, r = t.getElementsByTagName(e);
                if ("*" !== e) return r;
                for (; n = r[o++];) 1 === n.nodeType && i.push(n);
                return i
            }, w.find.CLASS = f.getElementsByClassName && function (e, t) {
                if (void 0 !== t.getElementsByClassName && S) return t.getElementsByClassName(e)
            }, u = [], v = [], (f.qsa = Z.test(k.querySelectorAll)) && (le(function (e) {
                a.appendChild(e).innerHTML = "<a id='" + C + "'></a><select id='" + C + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + I + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + I + "*(?:value|" + P + ")"), e.querySelectorAll("[id~=" + C + "-]").length || v.push("~="), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + C + "+*").length || v.push(".#.+[+~]")
            }), le(function (e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = k.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + I + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), a.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:")
            })), (f.matchesSelector = Z.test(g = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && le(function (e) {
                f.disconnectedMatch = g.call(e, "*"), g.call(e, "[s!='']:x"), u.push("!=", R)
            }), v = v.length && new RegExp(v.join("|")), u = u.length && new RegExp(u.join("|")), t = Z.test(a.compareDocumentPosition), y = t || Z.test(a.contains) ? function (e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
            } : function (e, t) {
                if (t) for (; t = t.parentNode;) if (t === e) return !0;
                return !1
            }, L = t ? function (e, t) {
                if (e === t) return c = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !f.sortDetached && t.compareDocumentPosition(e) === n ? e === k || e.ownerDocument === b && y(b, e) ? -1 : t === k || t.ownerDocument === b && y(b, t) ? 1 : l ? M(l, e) - M(l, t) : 0 : 4 & n ? -1 : 1)
            } : function (e, t) {
                if (e === t) return c = !0, 0;
                var n, i = 0, o = e.parentNode, r = t.parentNode, s = [e], a = [t];
                if (!o || !r) return e === k ? -1 : t === k ? 1 : o ? -1 : r ? 1 : l ? M(l, e) - M(l, t) : 0;
                if (o === r) return ue(e, t);
                for (n = e; n = n.parentNode;) s.unshift(n);
                for (n = t; n = n.parentNode;) a.unshift(n);
                for (; s[i] === a[i];) i++;
                return i ? ue(s[i], a[i]) : s[i] === b ? -1 : a[i] === b ? 1 : 0
            }), k
        }, re.matches = function (e, t) {
            return re(e, null, null, t)
        }, re.matchesSelector = function (e, t) {
            if ((e.ownerDocument || e) !== k && T(e), t = t.replace(U, "='$1']"), f.matchesSelector && S && !j[t + " "] && (!u || !u.test(t)) && (!v || !v.test(t))) try {
                var n = g.call(e, t);
                if (n || f.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
            } catch (e) {
            }
            return 0 < re(t, k, null, [e]).length
        }, re.contains = function (e, t) {
            return (e.ownerDocument || e) !== k && T(e), y(e, t)
        }, re.attr = function (e, t) {
            (e.ownerDocument || e) !== k && T(e);
            var n = w.attrHandle[t.toLowerCase()],
                i = n && D.call(w.attrHandle, t.toLowerCase()) ? n(e, t, !S) : void 0;
            return void 0 !== i ? i : f.attributes || !S ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }, re.escape = function (e) {
            return (e + "").replace(ne, ie)
        }, re.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, re.uniqueSort = function (e) {
            var t, n = [], i = 0, o = 0;
            if (c = !f.detectDuplicates, l = !f.sortStable && e.slice(0), e.sort(L), c) {
                for (; t = e[o++];) t === e[o] && (i = n.push(o));
                for (; i--;) e.splice(n[i], 1)
            }
            return l = null, e
        }, r = re.getText = function (e) {
            var t, n = "", i = 0, o = e.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += r(e)
                } else if (3 === o || 4 === o) return e.nodeValue
            } else for (; t = e[i++];) n += r(t);
            return n
        }, (w = re.selectors = {
            cacheLength: 50,
            createPseudo: ae,
            match: G,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (e) {
                    return e[1] = e[1].replace(te, d), e[3] = (e[3] || e[4] || e[5] || "").replace(te, d), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                }, CHILD: function (e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || re.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && re.error(e[0]), e
                }, PSEUDO: function (e) {
                    var t, n = !e[6] && e[2];
                    return G.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && Y.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function (e) {
                    var t = e.replace(te, d).toLowerCase();
                    return "*" === e ? function () {
                        return !0
                    } : function (e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                }, CLASS: function (e) {
                    var t = $[e + " "];
                    return t || (t = new RegExp("(^|" + I + ")" + e + "(" + I + "|$)")) && $(e, function (e) {
                        return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                    })
                }, ATTR: function (n, i, o) {
                    return function (e) {
                        var t = re.attr(e, n);
                        return null == t ? "!=" === i : !i || (t += "", "=" === i ? t === o : "!=" === i ? t !== o : "^=" === i ? o && 0 === t.indexOf(o) : "*=" === i ? o && -1 < t.indexOf(o) : "$=" === i ? o && t.slice(-o.length) === o : "~=" === i ? -1 < (" " + t.replace(W, " ") + " ").indexOf(o) : "|=" === i && (t === o || t.slice(0, o.length + 1) === o + "-"))
                    }
                }, CHILD: function (h, e, t, m, v) {
                    var g = "nth" !== h.slice(0, 3), y = "last" !== h.slice(-4), b = "of-type" === e;
                    return 1 === m && 0 === v ? function (e) {
                        return !!e.parentNode
                    } : function (e, t, n) {
                        var i, o, r, s, a, l, c = g != y ? "nextSibling" : "previousSibling", u = e.parentNode,
                            d = b && e.nodeName.toLowerCase(), p = !n && !b, f = !1;
                        if (u) {
                            if (g) {
                                for (; c;) {
                                    for (s = e; s = s[c];) if (b ? s.nodeName.toLowerCase() === d : 1 === s.nodeType) return !1;
                                    l = c = "only" === h && !l && "nextSibling"
                                }
                                return !0
                            }
                            if (l = [y ? u.firstChild : u.lastChild], y && p) {
                                for (f = (a = (i = (o = (r = (s = u)[C] || (s[C] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] || [])[0] === A && i[1]) && i[2], s = a && u.childNodes[a]; s = ++a && s && s[c] || (f = a = 0) || l.pop();) if (1 === s.nodeType && ++f && s === e) {
                                    o[h] = [A, a, f];
                                    break
                                }
                            } else if (p && (f = a = (i = (o = (r = (s = e)[C] || (s[C] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] || [])[0] === A && i[1]), !1 === f) for (; (s = ++a && s && s[c] || (f = a = 0) || l.pop()) && ((b ? s.nodeName.toLowerCase() !== d : 1 !== s.nodeType) || !++f || (p && ((o = (r = s[C] || (s[C] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] = [A, f]), s !== e));) ;
                            return (f -= v) === m || f % m == 0 && 0 <= f / m
                        }
                    }
                }, PSEUDO: function (e, r) {
                    var t, s = w.pseudos[e] || w.setFilters[e.toLowerCase()] || re.error("unsupported pseudo: " + e);
                    return s[C] ? s(r) : 1 < s.length ? (t = [e, e, "", r], w.setFilters.hasOwnProperty(e.toLowerCase()) ? ae(function (e, t) {
                        for (var n, i = s(e, r), o = i.length; o--;) e[n = M(e, i[o])] = !(t[n] = i[o])
                    }) : function (e) {
                        return s(e, 0, t)
                    }) : s
                }
            },
            pseudos: {
                not: ae(function (e) {
                    var i = [], o = [], a = p(e.replace(_, "$1"));
                    return a[C] ? ae(function (e, t, n, i) {
                        for (var o, r = a(e, null, i, []), s = e.length; s--;) (o = r[s]) && (e[s] = !(t[s] = o))
                    }) : function (e, t, n) {
                        return i[0] = e, a(i, null, n, o), i[0] = null, !o.pop()
                    }
                }), has: ae(function (t) {
                    return function (e) {
                        return 0 < re(t, e).length
                    }
                }), contains: ae(function (t) {
                    return t = t.replace(te, d), function (e) {
                        return -1 < (e.textContent || e.innerText || r(e)).indexOf(t)
                    }
                }), lang: ae(function (n) {
                    return V.test(n || "") || re.error("unsupported lang: " + n), n = n.replace(te, d).toLowerCase(), function (e) {
                        var t;
                        do {
                            if (t = S ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
                        } while ((e = e.parentNode) && 1 === e.nodeType);
                        return !1
                    }
                }), target: function (e) {
                    var t = n.location && n.location.hash;
                    return t && t.slice(1) === e.id
                }, root: function (e) {
                    return e === a
                }, focus: function (e) {
                    return e === k.activeElement && (!k.hasFocus || k.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                }, enabled: fe(!1), disabled: fe(!0), checked: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                }, selected: function (e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                }, empty: function (e) {
                    for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                    return !0
                }, parent: function (e) {
                    return !w.pseudos.empty(e)
                }, header: function (e) {
                    return Q.test(e.nodeName)
                }, input: function (e) {
                    return K.test(e.nodeName)
                }, button: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                }, text: function (e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                }, first: he(function () {
                    return [0]
                }), last: he(function (e, t) {
                    return [t - 1]
                }), eq: he(function (e, t, n) {
                    return [n < 0 ? n + t : n]
                }), even: he(function (e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e
                }), odd: he(function (e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e
                }), lt: he(function (e, t, n) {
                    for (var i = n < 0 ? n + t : n; 0 <= --i;) e.push(i);
                    return e
                }), gt: he(function (e, t, n) {
                    for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
                    return e
                })
            }
        }).pseudos.nth = w.pseudos.eq, {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) w.pseudos[e] = de(e);
        for (e in {submit: !0, reset: !0}) w.pseudos[e] = pe(e);

        function ve() {
        }

        function ge(e) {
            for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
            return i
        }

        function ye(a, e, t) {
            var l = e.dir, c = e.next, u = c || l, d = t && "parentNode" === u, p = i++;
            return e.first ? function (e, t, n) {
                for (; e = e[l];) if (1 === e.nodeType || d) return a(e, t, n);
                return !1
            } : function (e, t, n) {
                var i, o, r, s = [A, p];
                if (n) {
                    for (; e = e[l];) if ((1 === e.nodeType || d) && a(e, t, n)) return !0
                } else for (; e = e[l];) if (1 === e.nodeType || d) if (o = (r = e[C] || (e[C] = {}))[e.uniqueID] || (r[e.uniqueID] = {}), c && c === e.nodeName.toLowerCase()) e = e[l] || e; else {
                    if ((i = o[u]) && i[0] === A && i[1] === p) return s[2] = i[2];
                    if ((o[u] = s)[2] = a(e, t, n)) return !0
                }
                return !1
            }
        }

        function be(o) {
            return 1 < o.length ? function (e, t, n) {
                for (var i = o.length; i--;) if (!o[i](e, t, n)) return !1;
                return !0
            } : o[0]
        }

        function we(e, t, n, i, o) {
            for (var r, s = [], a = 0, l = e.length, c = null != t; a < l; a++) (r = e[a]) && (n && !n(r, i, o) || (s.push(r), c && t.push(a)));
            return s
        }

        function xe(f, h, m, v, g, e) {
            return v && !v[C] && (v = xe(v)), g && !g[C] && (g = xe(g, e)), ae(function (e, t, n, i) {
                var o, r, s, a = [], l = [], c = t.length, u = e || function (e, t, n) {
                        for (var i = 0, o = t.length; i < o; i++) re(e, t[i], n);
                        return n
                    }(h || "*", n.nodeType ? [n] : n, []), d = !f || !e && h ? u : we(u, a, f, n, i),
                    p = m ? g || (e ? f : c || v) ? [] : t : d;
                if (m && m(d, p, n, i), v) for (o = we(p, l), v(o, [], n, i), r = o.length; r--;) (s = o[r]) && (p[l[r]] = !(d[l[r]] = s));
                if (e) {
                    if (g || f) {
                        if (g) {
                            for (o = [], r = p.length; r--;) (s = p[r]) && o.push(d[r] = s);
                            g(null, p = [], o, i)
                        }
                        for (r = p.length; r--;) (s = p[r]) && -1 < (o = g ? M(e, s) : a[r]) && (e[o] = !(t[o] = s))
                    }
                } else p = we(p === t ? p.splice(c, p.length) : p), g ? g(null, t, p, i) : q.apply(t, p)
            })
        }

        function Te(e) {
            for (var o, t, n, i = e.length, r = w.relative[e[0].type], s = r || w.relative[" "], a = r ? 1 : 0, l = ye(function (e) {
                return e === o
            }, s, !0), c = ye(function (e) {
                return -1 < M(o, e)
            }, s, !0), u = [function (e, t, n) {
                var i = !r && (n || t !== x) || ((o = t).nodeType ? l(e, t, n) : c(e, t, n));
                return o = null, i
            }]; a < i; a++) if (t = w.relative[e[a].type]) u = [ye(be(u), t)]; else {
                if ((t = w.filter[e[a].type].apply(null, e[a].matches))[C]) {
                    for (n = ++a; n < i && !w.relative[e[n].type]; n++) ;
                    return xe(1 < a && be(u), 1 < a && ge(e.slice(0, a - 1).concat({value: " " === e[a - 2].type ? "*" : ""})).replace(_, "$1"), t, a < n && Te(e.slice(a, n)), n < i && Te(e = e.slice(n)), n < i && ge(e))
                }
                u.push(t)
            }
            return be(u)
        }

        return ve.prototype = w.filters = w.pseudos, w.setFilters = new ve, h = re.tokenize = function (e, t) {
            var n, i, o, r, s, a, l, c = E[e + " "];
            if (c) return t ? 0 : c.slice(0);
            for (s = e, a = [], l = w.preFilter; s;) {
                for (r in n && !(i = B.exec(s)) || (i && (s = s.slice(i[0].length) || s), a.push(o = [])), n = !1, (i = X.exec(s)) && (n = i.shift(), o.push({
                    value: n,
                    type: i[0].replace(_, " ")
                }), s = s.slice(n.length)), w.filter) !(i = G[r].exec(s)) || l[r] && !(i = l[r](i)) || (n = i.shift(), o.push({
                    value: n,
                    type: r,
                    matches: i
                }), s = s.slice(n.length));
                if (!n) break
            }
            return t ? s.length : s ? re.error(e) : E(e, a).slice(0)
        }, p = re.compile = function (e, t) {
            var n, i = [], o = [], r = j[e + " "];
            if (!r) {
                for (t || (t = h(e)), n = t.length; n--;) (r = Te(t[n]))[C] ? i.push(r) : o.push(r);
                (r = j(e, function (v, g) {
                    function e(e, t, n, i, o) {
                        var r, s, a, l = 0, c = "0", u = e && [], d = [], p = x, f = e || b && w.find.TAG("*", o),
                            h = A += null == p ? 1 : Math.random() || .1, m = f.length;
                        for (o && (x = t === k || t || o); c !== m && null != (r = f[c]); c++) {
                            if (b && r) {
                                for (s = 0, t || r.ownerDocument === k || (T(r), n = !S); a = v[s++];) if (a(r, t || k, n)) {
                                    i.push(r);
                                    break
                                }
                                o && (A = h)
                            }
                            y && ((r = !a && r) && l--, e && u.push(r))
                        }
                        if (l += c, y && c !== l) {
                            for (s = 0; a = g[s++];) a(u, d, t, n);
                            if (e) {
                                if (0 < l) for (; c--;) u[c] || d[c] || (d[c] = N.call(i));
                                d = we(d)
                            }
                            q.apply(i, d), o && !e && 0 < d.length && 1 < l + g.length && re.uniqueSort(i)
                        }
                        return o && (A = h, x = p), u
                    }

                    var y = 0 < g.length, b = 0 < v.length;
                    return y ? ae(e) : e
                }(o, i))).selector = e
            }
            return r
        }, m = re.select = function (e, t, n, i) {
            var o, r, s, a, l, c = "function" == typeof e && e, u = !i && h(e = c.selector || e);
            if (n = n || [], 1 === u.length) {
                if (2 < (r = u[0] = u[0].slice(0)).length && "ID" === (s = r[0]).type && 9 === t.nodeType && S && w.relative[r[1].type]) {
                    if (!(t = (w.find.ID(s.matches[0].replace(te, d), t) || [])[0])) return n;
                    c && (t = t.parentNode), e = e.slice(r.shift().value.length)
                }
                for (o = G.needsContext.test(e) ? 0 : r.length; o-- && (s = r[o], !w.relative[a = s.type]);) if ((l = w.find[a]) && (i = l(s.matches[0].replace(te, d), ee.test(r[0].type) && me(t.parentNode) || t))) {
                    if (r.splice(o, 1), !(e = i.length && ge(r))) return q.apply(n, i), n;
                    break
                }
            }
            return (c || p(e, u))(i, t, !S, n, !t || ee.test(e) && me(t.parentNode) || t), n
        }, f.sortStable = C.split("").sort(L).join("") === C, f.detectDuplicates = !!c, T(), f.sortDetached = le(function (e) {
            return 1 & e.compareDocumentPosition(k.createElement("fieldset"))
        }), le(function (e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || ce("type|href|height|width", function (e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), f.attributes && le(function (e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || ce("value", function (e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        }), le(function (e) {
            return null == e.getAttribute("disabled")
        }) || ce(P, function (e, t, n) {
            var i;
            if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }), re
    }(k);
    C.find = w, C.expr = w.selectors, C.expr[":"] = C.expr.pseudos, C.uniqueSort = C.unique = w.uniqueSort, C.text = w.getText, C.isXMLDoc = w.isXML, C.contains = w.contains, C.escapeSelector = w.escape;

    function x(e, t, n) {
        for (var i = [], o = void 0 !== n; (e = e[t]) && 9 !== e.nodeType;) if (1 === e.nodeType) {
            if (o && C(e).is(n)) break;
            i.push(e)
        }
        return i
    }

    function T(e, t) {
        for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
        return n
    }

    var A = C.expr.match.needsContext;

    function $(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }

    var E = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i, j = /^.[^:#\[\.,]*$/;

    function L(e, n, i) {
        return C.isFunction(n) ? C.grep(e, function (e, t) {
            return !!n.call(e, t, e) !== i
        }) : n.nodeType ? C.grep(e, function (e) {
            return e === n !== i
        }) : "string" != typeof n ? C.grep(e, function (e) {
            return -1 < o.call(n, e) !== i
        }) : j.test(n) ? C.filter(n, e, i) : (n = C.filter(n, e), C.grep(e, function (e) {
            return -1 < o.call(n, e) !== i && 1 === e.nodeType
        }))
    }

    C.filter = function (e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? C.find.matchesSelector(i, e) ? [i] : [] : C.find.matches(e, C.grep(t, function (e) {
            return 1 === e.nodeType
        }))
    }, C.fn.extend({
        find: function (e) {
            var t, n, i = this.length, o = this;
            if ("string" != typeof e) return this.pushStack(C(e).filter(function () {
                for (t = 0; t < i; t++) if (C.contains(o[t], this)) return !0
            }));
            for (n = this.pushStack([]), t = 0; t < i; t++) C.find(e, o[t], n);
            return 1 < i ? C.uniqueSort(n) : n
        }, filter: function (e) {
            return this.pushStack(L(this, e || [], !1))
        }, not: function (e) {
            return this.pushStack(L(this, e || [], !0))
        }, is: function (e) {
            return !!L(this, "string" == typeof e && A.test(e) ? C(e) : e || [], !1).length
        }
    });
    var D, N = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (C.fn.init = function (e, t, n) {
        var i, o;
        if (!e) return this;
        if (n = n || D, "string" != typeof e) return e.nodeType ? (this[0] = e, this.length = 1, this) : C.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(C) : C.makeArray(e, this);
        if (!(i = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : N.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
        if (i[1]) {
            if (t = t instanceof C ? t[0] : t, C.merge(this, C.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : S, !0)), E.test(i[1]) && C.isPlainObject(t)) for (i in t) C.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
            return this
        }
        return (o = S.getElementById(i[2])) && (this[0] = o, this.length = 1), this
    }).prototype = C.fn, D = C(S);
    var O = /^(?:parents|prev(?:Until|All))/, q = {children: !0, contents: !0, next: !0, prev: !0};

    function H(e, t) {
        for (; (e = e[t]) && 1 !== e.nodeType;) ;
        return e
    }

    C.fn.extend({
        has: function (e) {
            var t = C(e, this), n = t.length;
            return this.filter(function () {
                for (var e = 0; e < n; e++) if (C.contains(this, t[e])) return !0
            })
        }, closest: function (e, t) {
            var n, i = 0, o = this.length, r = [], s = "string" != typeof e && C(e);
            if (!A.test(e)) for (; i < o; i++) for (n = this[i]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (s ? -1 < s.index(n) : 1 === n.nodeType && C.find.matchesSelector(n, e))) {
                r.push(n);
                break
            }
            return this.pushStack(1 < r.length ? C.uniqueSort(r) : r)
        }, index: function (e) {
            return e ? "string" == typeof e ? o.call(C(e), this[0]) : o.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (e, t) {
            return this.pushStack(C.uniqueSort(C.merge(this.get(), C(e, t))))
        }, addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), C.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        }, parents: function (e) {
            return x(e, "parentNode")
        }, parentsUntil: function (e, t, n) {
            return x(e, "parentNode", n)
        }, next: function (e) {
            return H(e, "nextSibling")
        }, prev: function (e) {
            return H(e, "previousSibling")
        }, nextAll: function (e) {
            return x(e, "nextSibling")
        }, prevAll: function (e) {
            return x(e, "previousSibling")
        }, nextUntil: function (e, t, n) {
            return x(e, "nextSibling", n)
        }, prevUntil: function (e, t, n) {
            return x(e, "previousSibling", n)
        }, siblings: function (e) {
            return T((e.parentNode || {}).firstChild, e)
        }, children: function (e) {
            return T(e.firstChild)
        }, contents: function (e) {
            return $(e, "iframe") ? e.contentDocument : ($(e, "template") && (e = e.content || e), C.merge([], e.childNodes))
        }
    }, function (i, o) {
        C.fn[i] = function (e, t) {
            var n = C.map(this, o, e);
            return "Until" !== i.slice(-5) && (t = e), t && "string" == typeof t && (n = C.filter(t, n)), 1 < this.length && (q[i] || C.uniqueSort(n), O.test(i) && n.reverse()), this.pushStack(n)
        }
    });
    var M = /[^\x20\t\r\n\f]+/g;

    function P(e) {
        return e
    }

    function I(e) {
        throw e
    }

    function F(e, t, n, i) {
        var o;
        try {
            e && C.isFunction(o = e.promise) ? o.call(e).done(t).fail(n) : e && C.isFunction(o = e.then) ? o.call(e, t, n) : t.apply(void 0, [e].slice(i))
        } catch (e) {
            n.apply(void 0, [e])
        }
    }

    C.Callbacks = function (i) {
        i = "string" == typeof i ? function (e) {
            var n = {};
            return C.each(e.match(M) || [], function (e, t) {
                n[t] = !0
            }), n
        }(i) : C.extend({}, i);

        function n() {
            for (r = r || i.once, t = o = !0; a.length; l = -1) for (e = a.shift(); ++l < s.length;) !1 === s[l].apply(e[0], e[1]) && i.stopOnFalse && (l = s.length, e = !1);
            i.memory || (e = !1), o = !1, r && (s = e ? [] : "")
        }

        var o, e, t, r, s = [], a = [], l = -1, c = {
            add: function () {
                return s && (e && !o && (l = s.length - 1, a.push(e)), function n(e) {
                    C.each(e, function (e, t) {
                        C.isFunction(t) ? i.unique && c.has(t) || s.push(t) : t && t.length && "string" !== C.type(t) && n(t)
                    })
                }(arguments), e && !o && n()), this
            }, remove: function () {
                return C.each(arguments, function (e, t) {
                    for (var n; -1 < (n = C.inArray(t, s, n));) s.splice(n, 1), n <= l && l--
                }), this
            }, has: function (e) {
                return e ? -1 < C.inArray(e, s) : 0 < s.length
            }, empty: function () {
                return s && (s = []), this
            }, disable: function () {
                return r = a = [], s = e = "", this
            }, disabled: function () {
                return !s
            }, lock: function () {
                return r = a = [], e || o || (s = e = ""), this
            }, locked: function () {
                return !!r
            }, fireWith: function (e, t) {
                return r || (t = [e, (t = t || []).slice ? t.slice() : t], a.push(t), o || n()), this
            }, fire: function () {
                return c.fireWith(this, arguments), this
            }, fired: function () {
                return !!t
            }
        };
        return c
    }, C.extend({
        Deferred: function (e) {
            var r = [["notify", "progress", C.Callbacks("memory"), C.Callbacks("memory"), 2], ["resolve", "done", C.Callbacks("once memory"), C.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", C.Callbacks("once memory"), C.Callbacks("once memory"), 1, "rejected"]],
                o = "pending", s = {
                    state: function () {
                        return o
                    }, always: function () {
                        return a.done(arguments).fail(arguments), this
                    }, catch: function (e) {
                        return s.then(null, e)
                    }, pipe: function () {
                        var o = arguments;
                        return C.Deferred(function (i) {
                            C.each(r, function (e, t) {
                                var n = C.isFunction(o[t[4]]) && o[t[4]];
                                a[t[1]](function () {
                                    var e = n && n.apply(this, arguments);
                                    e && C.isFunction(e.promise) ? e.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[t[0] + "With"](this, n ? [e] : arguments)
                                })
                            }), o = null
                        }).promise()
                    }, then: function (t, n, i) {
                        var l = 0;

                        function c(o, r, s, a) {
                            return function () {
                                function e() {
                                    var e, t;
                                    if (!(o < l)) {
                                        if ((e = s.apply(n, i)) === r.promise()) throw new TypeError("Thenable self-resolution");
                                        t = e && ("object" == _typeof(e) || "function" == typeof e) && e.then, C.isFunction(t) ? a ? t.call(e, c(l, r, P, a), c(l, r, I, a)) : (l++, t.call(e, c(l, r, P, a), c(l, r, I, a), c(l, r, P, r.notifyWith))) : (s !== P && (n = void 0, i = [e]), (a || r.resolveWith)(n, i))
                                    }
                                }

                                var n = this, i = arguments, t = a ? e : function () {
                                    try {
                                        e()
                                    } catch (e) {
                                        C.Deferred.exceptionHook && C.Deferred.exceptionHook(e, t.stackTrace), l <= o + 1 && (s !== I && (n = void 0, i = [e]), r.rejectWith(n, i))
                                    }
                                };
                                o ? t() : (C.Deferred.getStackHook && (t.stackTrace = C.Deferred.getStackHook()), k.setTimeout(t))
                            }
                        }

                        return C.Deferred(function (e) {
                            r[0][3].add(c(0, e, C.isFunction(i) ? i : P, e.notifyWith)), r[1][3].add(c(0, e, C.isFunction(t) ? t : P)), r[2][3].add(c(0, e, C.isFunction(n) ? n : I))
                        }).promise()
                    }, promise: function (e) {
                        return null != e ? C.extend(e, s) : s
                    }
                }, a = {};
            return C.each(r, function (e, t) {
                var n = t[2], i = t[5];
                s[t[1]] = n.add, i && n.add(function () {
                    o = i
                }, r[3 - e][2].disable, r[0][2].lock), n.add(t[3].fire), a[t[0]] = function () {
                    return a[t[0] + "With"](this === a ? void 0 : this, arguments), this
                }, a[t[0] + "With"] = n.fireWith
            }), s.promise(a), e && e.call(a, a), a
        }, when: function (e) {
            function t(t) {
                return function (e) {
                    o[t] = this, r[t] = 1 < arguments.length ? a.call(arguments) : e, --n || s.resolveWith(o, r)
                }
            }

            var n = arguments.length, i = n, o = Array(i), r = a.call(arguments), s = C.Deferred();
            if (n <= 1 && (F(e, s.done(t(i)).resolve, s.reject, !n), "pending" === s.state() || C.isFunction(r[i] && r[i].then))) return s.then();
            for (; i--;) F(r[i], t(i), s.reject);
            return s.promise()
        }
    });
    var z = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    C.Deferred.exceptionHook = function (e, t) {
        k.console && k.console.warn && e && z.test(e.name) && k.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
    }, C.readyException = function (e) {
        k.setTimeout(function () {
            throw e
        })
    };
    var R = C.Deferred();

    function W() {
        S.removeEventListener("DOMContentLoaded", W), k.removeEventListener("load", W), C.ready()
    }

    C.fn.ready = function (e) {
        return R.then(e).catch(function (e) {
            C.readyException(e)
        }), this
    }, C.extend({
        isReady: !1, readyWait: 1, ready: function (e) {
            (!0 === e ? --C.readyWait : C.isReady) || ((C.isReady = !0) !== e && 0 < --C.readyWait || R.resolveWith(S, [C]))
        }
    }), C.ready.then = R.then, "complete" === S.readyState || "loading" !== S.readyState && !S.documentElement.doScroll ? k.setTimeout(C.ready) : (S.addEventListener("DOMContentLoaded", W), k.addEventListener("load", W));

    function _(e, t, n, i, o, r, s) {
        var a = 0, l = e.length, c = null == n;
        if ("object" === C.type(n)) for (a in o = !0, n) _(e, t, a, n[a], !0, r, s); else if (void 0 !== i && (o = !0, C.isFunction(i) || (s = !0), c && (t = s ? (t.call(e, i), null) : (c = t, function (e, t, n) {
            return c.call(C(e), n)
        })), t)) for (; a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
        return o ? e : c ? t.call(e) : l ? t(e[0], n) : r
    }

    function B(e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
    }

    function X() {
        this.expando = C.expando + X.uid++
    }

    X.uid = 1, X.prototype = {
        cache: function (e) {
            var t = e[this.expando];
            return t || (t = {}, B(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        }, set: function (e, t, n) {
            var i, o = this.cache(e);
            if ("string" == typeof t) o[C.camelCase(t)] = n; else for (i in t) o[C.camelCase(i)] = t[i];
            return o
        }, get: function (e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][C.camelCase(t)]
        }, access: function (e, t, n) {
            return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
        }, remove: function (e, t) {
            var n, i = e[this.expando];
            if (void 0 !== i) {
                if (void 0 !== t) {
                    n = (t = Array.isArray(t) ? t.map(C.camelCase) : (t = C.camelCase(t)) in i ? [t] : t.match(M) || []).length;
                    for (; n--;) delete i[t[n]]
                }
                void 0 !== t && !C.isEmptyObject(i) || (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        }, hasData: function (e) {
            var t = e[this.expando];
            return void 0 !== t && !C.isEmptyObject(t)
        }
    };
    var U = new X, Y = new X, V = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, G = /[A-Z]/g;

    function K(e, t, n) {
        var i;
        if (void 0 === n && 1 === e.nodeType) if (i = "data-" + t.replace(G, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(i))) {
            try {
                n = function (e) {
                    return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : V.test(e) ? JSON.parse(e) : e)
                }(n)
            } catch (e) {
            }
            Y.set(e, t, n)
        } else n = void 0;
        return n
    }

    C.extend({
        hasData: function (e) {
            return Y.hasData(e) || U.hasData(e)
        }, data: function (e, t, n) {
            return Y.access(e, t, n)
        }, removeData: function (e, t) {
            Y.remove(e, t)
        }, _data: function (e, t, n) {
            return U.access(e, t, n)
        }, _removeData: function (e, t) {
            U.remove(e, t)
        }
    }), C.fn.extend({
        data: function (n, e) {
            var t, i, o, r = this[0], s = r && r.attributes;
            if (void 0 !== n) return "object" == _typeof(n) ? this.each(function () {
                Y.set(this, n)
            }) : _(this, function (e) {
                var t;
                if (r && void 0 === e) {
                    if (void 0 !== (t = Y.get(r, n))) return t;
                    if (void 0 !== (t = K(r, n))) return t
                } else this.each(function () {
                    Y.set(this, n, e)
                })
            }, null, e, 1 < arguments.length, null, !0);
            if (this.length && (o = Y.get(r), 1 === r.nodeType && !U.get(r, "hasDataAttrs"))) {
                for (t = s.length; t--;) s[t] && (0 === (i = s[t].name).indexOf("data-") && (i = C.camelCase(i.slice(5)), K(r, i, o[i])));
                U.set(r, "hasDataAttrs", !0)
            }
            return o
        }, removeData: function (e) {
            return this.each(function () {
                Y.remove(this, e)
            })
        }
    }), C.extend({
        queue: function (e, t, n) {
            var i;
            if (e) return t = (t || "fx") + "queue", i = U.get(e, t), n && (!i || Array.isArray(n) ? i = U.access(e, t, C.makeArray(n)) : i.push(n)), i || []
        }, dequeue: function (e, t) {
            t = t || "fx";
            var n = C.queue(e, t), i = n.length, o = n.shift(), r = C._queueHooks(e, t);
            "inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, function () {
                C.dequeue(e, t)
            }, r)), !i && r && r.empty.fire()
        }, _queueHooks: function (e, t) {
            var n = t + "queueHooks";
            return U.get(e, n) || U.access(e, n, {
                empty: C.Callbacks("once memory").add(function () {
                    U.remove(e, [t + "queue", n])
                })
            })
        }
    }), C.fn.extend({
        queue: function (t, n) {
            var e = 2;
            return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? C.queue(this[0], t) : void 0 === n ? this : this.each(function () {
                var e = C.queue(this, t, n);
                C._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && C.dequeue(this, t)
            })
        }, dequeue: function (e) {
            return this.each(function () {
                C.dequeue(this, e)
            })
        }, clearQueue: function (e) {
            return this.queue(e || "fx", [])
        }, promise: function (e, t) {
            function n() {
                --o || r.resolveWith(s, [s])
            }

            var i, o = 1, r = C.Deferred(), s = this, a = this.length;
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;) (i = U.get(s[a], e + "queueHooks")) && i.empty && (o++, i.empty.add(n));
            return n(), r.promise(t)
        }
    });

    function Q(e, t, n, i) {
        var o, r, s = {};
        for (r in t) s[r] = e.style[r], e.style[r] = t[r];
        for (r in o = n.apply(e, i || []), t) e.style[r] = s[r];
        return o
    }

    var Z = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, J = new RegExp("^(?:([+-])=|)(" + Z + ")([a-z%]*)$", "i"),
        ee = ["Top", "Right", "Bottom", "Left"], te = function (e, t) {
            return "none" === (e = t || e).style.display || "" === e.style.display && C.contains(e.ownerDocument, e) && "none" === C.css(e, "display")
        };

    function ne(e, t, n, i) {
        var o, r = 1, s = 20, a = i ? function () {
                return i.cur()
            } : function () {
                return C.css(e, t, "")
            }, l = a(), c = n && n[3] || (C.cssNumber[t] ? "" : "px"),
            u = (C.cssNumber[t] || "px" !== c && +l) && J.exec(C.css(e, t));
        if (u && u[3] !== c) for (c = c || u[3], n = n || [], u = +l || 1; u /= r = r || ".5", C.style(e, t, u + c), r !== (r = a() / l) && 1 !== r && --s;) ;
        return n && (u = +u || +l || 0, o = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = o)), o
    }

    var ie = {};

    function oe(e, t) {
        for (var n, i, o = [], r = 0, s = e.length; r < s; r++) (i = e[r]).style && (n = i.style.display, t ? ("none" === n && (o[r] = U.get(i, "display") || null, o[r] || (i.style.display = "")), "" === i.style.display && te(i) && (o[r] = (d = c = l = void 0, c = (a = i).ownerDocument, u = a.nodeName, (d = ie[u]) || (l = c.body.appendChild(c.createElement(u)), d = C.css(l, "display"), l.parentNode.removeChild(l), "none" === d && (d = "block"), ie[u] = d)))) : "none" !== n && (o[r] = "none", U.set(i, "display", n)));
        var a, l, c, u, d;
        for (r = 0; r < s; r++) null != o[r] && (e[r].style.display = o[r]);
        return e
    }

    C.fn.extend({
        show: function () {
            return oe(this, !0)
        }, hide: function () {
            return oe(this)
        }, toggle: function (e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
                te(this) ? C(this).show() : C(this).hide()
            })
        }
    });
    var re = /^(?:checkbox|radio)$/i, se = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i, ae = /^$|\/(?:java|ecma)script/i, le = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };

    function ce(e, t) {
        var n;
        return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && $(e, t) ? C.merge([e], n) : n
    }

    function ue(e, t) {
        for (var n = 0, i = e.length; n < i; n++) U.set(e[n], "globalEval", !t || U.get(t[n], "globalEval"))
    }

    le.optgroup = le.option, le.tbody = le.tfoot = le.colgroup = le.caption = le.thead, le.th = le.td;
    var de, pe, fe = /<|&#?\w+;/;

    function he(e, t, n, i, o) {
        for (var r, s, a, l, c, u, d = t.createDocumentFragment(), p = [], f = 0, h = e.length; f < h; f++) if ((r = e[f]) || 0 === r) if ("object" === C.type(r)) C.merge(p, r.nodeType ? [r] : r); else if (fe.test(r)) {
            for (s = s || d.appendChild(t.createElement("div")), a = (se.exec(r) || ["", ""])[1].toLowerCase(), l = le[a] || le._default, s.innerHTML = l[1] + C.htmlPrefilter(r) + l[2], u = l[0]; u--;) s = s.lastChild;
            C.merge(p, s.childNodes), (s = d.firstChild).textContent = ""
        } else p.push(t.createTextNode(r));
        for (d.textContent = "", f = 0; r = p[f++];) if (i && -1 < C.inArray(r, i)) o && o.push(r); else if (c = C.contains(r.ownerDocument, r), s = ce(d.appendChild(r), "script"), c && ue(s), n) for (u = 0; r = s[u++];) ae.test(r.type || "") && n.push(r);
        return d
    }

    de = S.createDocumentFragment().appendChild(S.createElement("div")), (pe = S.createElement("input")).setAttribute("type", "radio"), pe.setAttribute("checked", "checked"), pe.setAttribute("name", "t"), de.appendChild(pe), v.checkClone = de.cloneNode(!0).cloneNode(!0).lastChild.checked, de.innerHTML = "<textarea>x</textarea>", v.noCloneChecked = !!de.cloneNode(!0).lastChild.defaultValue;
    var me = S.documentElement, ve = /^key/, ge = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        ye = /^([^.]*)(?:\.(.+)|)/;

    function be() {
        return !0
    }

    function we() {
        return !1
    }

    function xe() {
        try {
            return S.activeElement
        } catch (e) {
        }
    }

    function Te(e, t, n, i, o, r) {
        var s, a;
        if ("object" == _typeof(t)) {
            for (a in "string" != typeof n && (i = i || n, n = void 0), t) Te(e, a, n, i, t[a], r);
            return e
        }
        if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), !1 === o) o = we; else if (!o) return e;
        return 1 === r && (s = o, (o = function (e) {
            return C().off(e), s.apply(this, arguments)
        }).guid = s.guid || (s.guid = C.guid++)), e.each(function () {
            C.event.add(this, t, o, i, n)
        })
    }

    C.event = {
        global: {}, add: function (t, e, n, i, o) {
            var r, s, a, l, c, u, d, p, f, h, m, v = U.get(t);
            if (v) for (n.handler && (n = (r = n).handler, o = r.selector), o && C.find.matchesSelector(me, o), n.guid || (n.guid = C.guid++), (l = v.events) || (l = v.events = {}), (s = v.handle) || (s = v.handle = function (e) {
                return void 0 !== C && C.event.triggered !== e.type ? C.event.dispatch.apply(t, arguments) : void 0
            }), c = (e = (e || "").match(M) || [""]).length; c--;) f = m = (a = ye.exec(e[c]) || [])[1], h = (a[2] || "").split(".").sort(), f && (d = C.event.special[f] || {}, f = (o ? d.delegateType : d.bindType) || f, d = C.event.special[f] || {}, u = C.extend({
                type: f,
                origType: m,
                data: i,
                handler: n,
                guid: n.guid,
                selector: o,
                needsContext: o && C.expr.match.needsContext.test(o),
                namespace: h.join(".")
            }, r), (p = l[f]) || ((p = l[f] = []).delegateCount = 0, d.setup && !1 !== d.setup.call(t, i, h, s) || t.addEventListener && t.addEventListener(f, s)), d.add && (d.add.call(t, u), u.handler.guid || (u.handler.guid = n.guid)), o ? p.splice(p.delegateCount++, 0, u) : p.push(u), C.event.global[f] = !0)
        }, remove: function (e, t, n, i, o) {
            var r, s, a, l, c, u, d, p, f, h, m, v = U.hasData(e) && U.get(e);
            if (v && (l = v.events)) {
                for (c = (t = (t || "").match(M) || [""]).length; c--;) if (f = m = (a = ye.exec(t[c]) || [])[1], h = (a[2] || "").split(".").sort(), f) {
                    for (d = C.event.special[f] || {}, p = l[f = (i ? d.delegateType : d.bindType) || f] || [], a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = p.length; r--;) u = p[r], !o && m !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (p.splice(r, 1), u.selector && p.delegateCount--, d.remove && d.remove.call(e, u));
                    s && !p.length && (d.teardown && !1 !== d.teardown.call(e, h, v.handle) || C.removeEvent(e, f, v.handle), delete l[f])
                } else for (f in l) C.event.remove(e, f + t[c], n, i, !0);
                C.isEmptyObject(l) && U.remove(e, "handle events")
            }
        }, dispatch: function (e) {
            var t, n, i, o, r, s, a = C.event.fix(e), l = new Array(arguments.length),
                c = (U.get(this, "events") || {})[a.type] || [], u = C.event.special[a.type] || {};
            for (l[0] = a, t = 1; t < arguments.length; t++) l[t] = arguments[t];
            if (a.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
                for (s = C.event.handlers.call(this, a, c), t = 0; (o = s[t++]) && !a.isPropagationStopped();) for (a.currentTarget = o.elem, n = 0; (r = o.handlers[n++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (i = ((C.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, a), a.result
            }
        }, handlers: function (e, t) {
            var n, i, o, r, s, a = [], l = t.delegateCount, c = e.target;
            if (l && c.nodeType && !("click" === e.type && 1 <= e.button)) for (; c !== this; c = c.parentNode || this) if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
                for (r = [], s = {}, n = 0; n < l; n++) void 0 === s[o = (i = t[n]).selector + " "] && (s[o] = i.needsContext ? -1 < C(o, this).index(c) : C.find(o, this, null, [c]).length), s[o] && r.push(i);
                r.length && a.push({elem: c, handlers: r})
            }
            return c = this, l < t.length && a.push({elem: c, handlers: t.slice(l)}), a
        }, addProp: function (t, e) {
            Object.defineProperty(C.Event.prototype, t, {
                enumerable: !0,
                configurable: !0,
                get: C.isFunction(e) ? function () {
                    if (this.originalEvent) return e(this.originalEvent)
                } : function () {
                    if (this.originalEvent) return this.originalEvent[t]
                },
                set: function (e) {
                    Object.defineProperty(this, t, {enumerable: !0, configurable: !0, writable: !0, value: e})
                }
            })
        }, fix: function (e) {
            return e[C.expando] ? e : new C.Event(e)
        }, special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== xe() && this.focus) return this.focus(), !1
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    if (this === xe() && this.blur) return this.blur(), !1
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    if ("checkbox" === this.type && this.click && $(this, "input")) return this.click(), !1
                }, _default: function (e) {
                    return $(e.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, C.removeEvent = function (e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, C.Event = function (e, t) {
        return this instanceof C.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? be : we, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && C.extend(this, t), this.timeStamp = e && e.timeStamp || C.now(), void (this[C.expando] = !0)) : new C.Event(e, t)
    }, C.Event.prototype = {
        constructor: C.Event,
        isDefaultPrevented: we,
        isPropagationStopped: we,
        isImmediatePropagationStopped: we,
        isSimulated: !1,
        preventDefault: function () {
            var e = this.originalEvent;
            this.isDefaultPrevented = be, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function () {
            var e = this.originalEvent;
            this.isPropagationStopped = be, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function () {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = be, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, C.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (e) {
            var t = e.button;
            return null == e.which && ve.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && ge.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, C.event.addProp), C.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (e, o) {
        C.event.special[e] = {
            delegateType: o, bindType: o, handle: function (e) {
                var t, n = e.relatedTarget, i = e.handleObj;
                return n && (n === this || C.contains(this, n)) || (e.type = i.origType, t = i.handler.apply(this, arguments), e.type = o), t
            }
        }
    }), C.fn.extend({
        on: function (e, t, n, i) {
            return Te(this, e, t, n, i)
        }, one: function (e, t, n, i) {
            return Te(this, e, t, n, i, 1)
        }, off: function (e, t, n) {
            var i, o;
            if (e && e.preventDefault && e.handleObj) return i = e.handleObj, C(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" != _typeof(e)) return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = we), this.each(function () {
                C.event.remove(this, e, n, t)
            });
            for (o in e) this.off(o, t, e[o]);
            return this
        }
    });
    var ke = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        Se = /<script|<style|<link/i, Ce = /checked\s*(?:[^=]|=\s*.checked.)/i, Ae = /^true\/(.*)/,
        $e = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function Ee(e, t) {
        return $(e, "table") && $(11 !== t.nodeType ? t : t.firstChild, "tr") && C(">tbody", e)[0] || e
    }

    function je(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function Le(e) {
        var t = Ae.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function De(e, t) {
        var n, i, o, r, s, a, l, c;
        if (1 === t.nodeType) {
            if (U.hasData(e) && (r = U.access(e), s = U.set(t, r), c = r.events)) for (o in delete s.handle, s.events = {}, c) for (n = 0, i = c[o].length; n < i; n++) C.event.add(t, o, c[o][n]);
            Y.hasData(e) && (a = Y.access(e), l = C.extend({}, a), Y.set(t, l))
        }
    }

    function Ne(n, i, o, r) {
        i = m.apply([], i);
        var e, t, s, a, l, c, u = 0, d = n.length, p = d - 1, f = i[0], h = C.isFunction(f);
        if (h || 1 < d && "string" == typeof f && !v.checkClone && Ce.test(f)) return n.each(function (e) {
            var t = n.eq(e);
            h && (i[0] = f.call(this, e, t.html())), Ne(t, i, o, r)
        });
        if (d && (t = (e = he(i, n[0].ownerDocument, !1, n, r)).firstChild, 1 === e.childNodes.length && (e = t), t || r)) {
            for (a = (s = C.map(ce(e, "script"), je)).length; u < d; u++) l = e, u !== p && (l = C.clone(l, !0, !0), a && C.merge(s, ce(l, "script"))), o.call(n[u], l, u);
            if (a) for (c = s[s.length - 1].ownerDocument, C.map(s, Le), u = 0; u < a; u++) l = s[u], ae.test(l.type || "") && !U.access(l, "globalEval") && C.contains(c, l) && (l.src ? C._evalUrl && C._evalUrl(l.src) : g(l.textContent.replace($e, ""), c))
        }
        return n
    }

    function Oe(e, t, n) {
        for (var i, o = t ? C.filter(t, e) : e, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || C.cleanData(ce(i)), i.parentNode && (n && C.contains(i.ownerDocument, i) && ue(ce(i, "script")), i.parentNode.removeChild(i));
        return e
    }

    C.extend({
        htmlPrefilter: function (e) {
            return e.replace(ke, "<$1></$2>")
        }, clone: function (e, t, n) {
            var i, o, r, s, a, l, c, u = e.cloneNode(!0), d = C.contains(e.ownerDocument, e);
            if (!(v.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || C.isXMLDoc(e))) for (s = ce(u), i = 0, o = (r = ce(e)).length; i < o; i++) a = r[i], l = s[i], void 0, "input" === (c = l.nodeName.toLowerCase()) && re.test(a.type) ? l.checked = a.checked : "input" !== c && "textarea" !== c || (l.defaultValue = a.defaultValue);
            if (t) if (n) for (r = r || ce(e), s = s || ce(u), i = 0, o = r.length; i < o; i++) De(r[i], s[i]); else De(e, u);
            return 0 < (s = ce(u, "script")).length && ue(s, !d && ce(e, "script")), u
        }, cleanData: function (e) {
            for (var t, n, i, o = C.event.special, r = 0; void 0 !== (n = e[r]); r++) if (B(n)) {
                if (t = n[U.expando]) {
                    if (t.events) for (i in t.events) o[i] ? C.event.remove(n, i) : C.removeEvent(n, i, t.handle);
                    n[U.expando] = void 0
                }
                n[Y.expando] && (n[Y.expando] = void 0)
            }
        }
    }), C.fn.extend({
        detach: function (e) {
            return Oe(this, e, !0)
        }, remove: function (e) {
            return Oe(this, e)
        }, text: function (e) {
            return _(this, function (e) {
                return void 0 === e ? C.text(this) : this.empty().each(function () {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        }, append: function () {
            return Ne(this, arguments, function (e) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Ee(this, e).appendChild(e)
            })
        }, prepend: function () {
            return Ne(this, arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = Ee(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        }, before: function () {
            return Ne(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        }, after: function () {
            return Ne(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        }, empty: function () {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (C.cleanData(ce(e, !1)), e.textContent = "");
            return this
        }, clone: function (e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function () {
                return C.clone(this, e, t)
            })
        }, html: function (e) {
            return _(this, function (e) {
                var t = this[0] || {}, n = 0, i = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !Se.test(e) && !le[(se.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = C.htmlPrefilter(e);
                    try {
                        for (; n < i; n++) 1 === (t = this[n] || {}).nodeType && (C.cleanData(ce(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {
                    }
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        }, replaceWith: function () {
            var n = [];
            return Ne(this, arguments, function (e) {
                var t = this.parentNode;
                C.inArray(this, n) < 0 && (C.cleanData(ce(this)), t && t.replaceChild(e, this))
            }, n)
        }
    }), C.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (e, s) {
        C.fn[e] = function (e) {
            for (var t, n = [], i = C(e), o = i.length - 1, r = 0; r <= o; r++) t = r === o ? this : this.clone(!0), C(i[r])[s](t), l.apply(n, t.get());
            return this.pushStack(n)
        }
    });
    var qe, He, Me, Pe, Ie, Fe, ze = /^margin/, Re = new RegExp("^(" + Z + ")(?!px)[a-z%]+$", "i"), We = function (e) {
        var t = e.ownerDocument.defaultView;
        return t && t.opener || (t = k), t.getComputedStyle(e)
    };

    function _e() {
        if (Fe) {
            Fe.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", Fe.innerHTML = "", me.appendChild(Ie);
            var e = k.getComputedStyle(Fe);
            qe = "1%" !== e.top, Pe = "2px" === e.marginLeft, He = "4px" === e.width, Fe.style.marginRight = "50%", Me = "4px" === e.marginRight, me.removeChild(Ie), Fe = null
        }
    }

    function Be(e, t, n) {
        var i, o, r, s, a = e.style;
        return (n = n || We(e)) && ("" !== (s = n.getPropertyValue(t) || n[t]) || C.contains(e.ownerDocument, e) || (s = C.style(e, t)), !v.pixelMarginRight() && Re.test(s) && ze.test(t) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
    }

    function Xe(e, t) {
        return {
            get: function () {
                return e() ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    Ie = S.createElement("div"), (Fe = S.createElement("div")).style && (Fe.style.backgroundClip = "content-box", Fe.cloneNode(!0).style.backgroundClip = "", v.clearCloneStyle = "content-box" === Fe.style.backgroundClip, Ie.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", Ie.appendChild(Fe), C.extend(v, {
        pixelPosition: function () {
            return _e(), qe
        }, boxSizingReliable: function () {
            return _e(), He
        }, pixelMarginRight: function () {
            return _e(), Me
        }, reliableMarginLeft: function () {
            return _e(), Pe
        }
    }));
    var Ue = /^(none|table(?!-c[ea]).+)/, Ye = /^--/,
        Ve = {position: "absolute", visibility: "hidden", display: "block"},
        Ge = {letterSpacing: "0", fontWeight: "400"}, Ke = ["Webkit", "Moz", "ms"], Qe = S.createElement("div").style;

    function Ze(e) {
        var t = C.cssProps[e];
        return t || (t = C.cssProps[e] = function (e) {
            if (e in Qe) return e;
            for (var t = e[0].toUpperCase() + e.slice(1), n = Ke.length; n--;) if ((e = Ke[n] + t) in Qe) return e
        }(e) || e), t
    }

    function Je(e, t, n) {
        var i = J.exec(t);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
    }

    function et(e, t, n, i, o) {
        var r, s = 0;
        for (r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0; r < 4; r += 2) "margin" === n && (s += C.css(e, n + ee[r], !0, o)), i ? ("content" === n && (s -= C.css(e, "padding" + ee[r], !0, o)), "margin" !== n && (s -= C.css(e, "border" + ee[r] + "Width", !0, o))) : (s += C.css(e, "padding" + ee[r], !0, o), "padding" !== n && (s += C.css(e, "border" + ee[r] + "Width", !0, o)));
        return s
    }

    function tt(e, t, n) {
        var i, o = We(e), r = Be(e, t, o), s = "border-box" === C.css(e, "boxSizing", !1, o);
        return Re.test(r) ? r : (i = s && (v.boxSizingReliable() || r === e.style[t]), "auto" === r && (r = e["offset" + t[0].toUpperCase() + t.slice(1)]), (r = parseFloat(r) || 0) + et(e, t, n || (s ? "border" : "content"), i, o) + "px")
    }

    function nt(e, t, n, i, o) {
        return new nt.prototype.init(e, t, n, i, o)
    }

    C.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = Be(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {float: "cssFloat"},
        style: function (e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, r, s, a = C.camelCase(t), l = Ye.test(t), c = e.style;
                return l || (t = Ze(a)), s = C.cssHooks[t] || C.cssHooks[a], void 0 === n ? s && "get" in s && void 0 !== (o = s.get(e, !1, i)) ? o : c[t] : ("string" === (r = _typeof(n)) && (o = J.exec(n)) && o[1] && (n = ne(e, t, o), r = "number"), void (null != n && n == n && ("number" === r && (n += o && o[3] || (C.cssNumber[a] ? "" : "px")), v.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l ? c.setProperty(t, n) : c[t] = n))))
            }
        },
        css: function (e, t, n, i) {
            var o, r, s, a = C.camelCase(t);
            return Ye.test(t) || (t = Ze(a)), (s = C.cssHooks[t] || C.cssHooks[a]) && "get" in s && (o = s.get(e, !0, n)), void 0 === o && (o = Be(e, t, i)), "normal" === o && t in Ge && (o = Ge[t]), "" === n || n ? (r = parseFloat(o), !0 === n || isFinite(r) ? r || 0 : o) : o
        }
    }), C.each(["height", "width"], function (e, s) {
        C.cssHooks[s] = {
            get: function (e, t, n) {
                if (t) return !Ue.test(C.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? tt(e, s, n) : Q(e, Ve, function () {
                    return tt(e, s, n)
                })
            }, set: function (e, t, n) {
                var i, o = n && We(e), r = n && et(e, s, n, "border-box" === C.css(e, "boxSizing", !1, o), o);
                return r && (i = J.exec(t)) && "px" !== (i[3] || "px") && (e.style[s] = t, t = C.css(e, s)), Je(0, t, r)
            }
        }
    }), C.cssHooks.marginLeft = Xe(v.reliableMarginLeft, function (e, t) {
        if (t) return (parseFloat(Be(e, "marginLeft")) || e.getBoundingClientRect().left - Q(e, {marginLeft: 0}, function () {
            return e.getBoundingClientRect().left
        })) + "px"
    }), C.each({margin: "", padding: "", border: "Width"}, function (o, r) {
        C.cssHooks[o + r] = {
            expand: function (e) {
                for (var t = 0, n = {}, i = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[o + ee[t] + r] = i[t] || i[t - 2] || i[0];
                return n
            }
        }, ze.test(o) || (C.cssHooks[o + r].set = Je)
    }), C.fn.extend({
        css: function (e, t) {
            return _(this, function (e, t, n) {
                var i, o, r = {}, s = 0;
                if (Array.isArray(t)) {
                    for (i = We(e), o = t.length; s < o; s++) r[t[s]] = C.css(e, t[s], !1, i);
                    return r
                }
                return void 0 !== n ? C.style(e, t, n) : C.css(e, t)
            }, e, t, 1 < arguments.length)
        }
    }), ((C.Tween = nt).prototype = {
        constructor: nt, init: function (e, t, n, i, o, r) {
            this.elem = e, this.prop = n, this.easing = o || C.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (C.cssNumber[n] ? "" : "px")
        }, cur: function () {
            var e = nt.propHooks[this.prop];
            return e && e.get ? e.get(this) : nt.propHooks._default.get(this)
        }, run: function (e) {
            var t, n = nt.propHooks[this.prop];
            return this.options.duration ? this.pos = t = C.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : nt.propHooks._default.set(this), this
        }
    }).init.prototype = nt.prototype, (nt.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = C.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
            }, set: function (e) {
                C.fx.step[e.prop] ? C.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[C.cssProps[e.prop]] && !C.cssHooks[e.prop] ? e.elem[e.prop] = e.now : C.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }).scrollTop = nt.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, C.easing = {
        linear: function (e) {
            return e
        }, swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }, _default: "swing"
    }, C.fx = nt.prototype.init, C.fx.step = {};
    var it, ot, rt, st, at = /^(?:toggle|show|hide)$/, lt = /queueHooks$/;

    function ct() {
        ot && (!1 === S.hidden && k.requestAnimationFrame ? k.requestAnimationFrame(ct) : k.setTimeout(ct, C.fx.interval), C.fx.tick())
    }

    function ut() {
        return k.setTimeout(function () {
            it = void 0
        }), it = C.now()
    }

    function dt(e, t) {
        var n, i = 0, o = {height: e};
        for (t = t ? 1 : 0; i < 4; i += 2 - t) o["margin" + (n = ee[i])] = o["padding" + n] = e;
        return t && (o.opacity = o.width = e), o
    }

    function pt(e, t, n) {
        for (var i, o = (ft.tweeners[t] || []).concat(ft.tweeners["*"]), r = 0, s = o.length; r < s; r++) if (i = o[r].call(n, t, e)) return i
    }

    function ft(r, e, t) {
        var n, s, i = 0, o = ft.prefilters.length, a = C.Deferred().always(function () {
            delete l.elem
        }), l = function () {
            if (s) return !1;
            for (var e = it || ut(), t = Math.max(0, c.startTime + c.duration - e), n = 1 - (t / c.duration || 0), i = 0, o = c.tweens.length; i < o; i++) c.tweens[i].run(n);
            return a.notifyWith(r, [c, n, t]), n < 1 && o ? t : (o || a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c]), !1)
        }, c = a.promise({
            elem: r,
            props: C.extend({}, e),
            opts: C.extend(!0, {specialEasing: {}, easing: C.easing._default}, t),
            originalProperties: e,
            originalOptions: t,
            startTime: it || ut(),
            duration: t.duration,
            tweens: [],
            createTween: function (e, t) {
                var n = C.Tween(r, c.opts, e, t, c.opts.specialEasing[e] || c.opts.easing);
                return c.tweens.push(n), n
            },
            stop: function (e) {
                var t = 0, n = e ? c.tweens.length : 0;
                if (s) return this;
                for (s = !0; t < n; t++) c.tweens[t].run(1);
                return e ? (a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c, e])) : a.rejectWith(r, [c, e]), this
            }
        }), u = c.props;
        for (function (e, t) {
            var n, i, o, r, s;
            for (n in e) if (o = t[i = C.camelCase(n)], r = e[n], Array.isArray(r) && (o = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), (s = C.cssHooks[i]) && "expand" in s) for (n in r = s.expand(r), delete e[i], r) n in e || (e[n] = r[n], t[n] = o); else t[i] = o
        }(u, c.opts.specialEasing); i < o; i++) if (n = ft.prefilters[i].call(c, r, u, c.opts)) return C.isFunction(n.stop) && (C._queueHooks(c.elem, c.opts.queue).stop = C.proxy(n.stop, n)), n;
        return C.map(u, pt, c), C.isFunction(c.opts.start) && c.opts.start.call(r, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), C.fx.timer(C.extend(l, {
            elem: r,
            anim: c,
            queue: c.opts.queue
        })), c
    }

    C.Animation = C.extend(ft, {
        tweeners: {
            "*": [function (e, t) {
                var n = this.createTween(e, t);
                return ne(n.elem, e, J.exec(t), n), n
            }]
        }, tweener: function (e, t) {
            for (var n, i = 0, o = (e = C.isFunction(e) ? (t = e, ["*"]) : e.match(M)).length; i < o; i++) n = e[i], ft.tweeners[n] = ft.tweeners[n] || [], ft.tweeners[n].unshift(t)
        }, prefilters: [function (e, t, n) {
            var i, o, r, s, a, l, c, u, d = "width" in t || "height" in t, p = this, f = {}, h = e.style,
                m = e.nodeType && te(e), v = U.get(e, "fxshow");
            for (i in n.queue || (null == (s = C._queueHooks(e, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function () {
                s.unqueued || a()
            }), s.unqueued++, p.always(function () {
                p.always(function () {
                    s.unqueued--, C.queue(e, "fx").length || s.empty.fire()
                })
            })), t) if (o = t[i], at.test(o)) {
                if (delete t[i], r = r || "toggle" === o, o === (m ? "hide" : "show")) {
                    if ("show" !== o || !v || void 0 === v[i]) continue;
                    m = !0
                }
                f[i] = v && v[i] || C.style(e, i)
            }
            if ((l = !C.isEmptyObject(t)) || !C.isEmptyObject(f)) for (i in d && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (c = v && v.display) && (c = U.get(e, "display")), "none" === (u = C.css(e, "display")) && (c ? u = c : (oe([e], !0), c = e.style.display || c, u = C.css(e, "display"), oe([e]))), ("inline" === u || "inline-block" === u && null != c) && "none" === C.css(e, "float") && (l || (p.done(function () {
                h.display = c
            }), null == c && (u = h.display, c = "none" === u ? "" : u)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
                h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
            })), l = !1, f) l || (v ? "hidden" in v && (m = v.hidden) : v = U.access(e, "fxshow", {display: c}), r && (v.hidden = !m), m && oe([e], !0), p.done(function () {
                for (i in m || oe([e]), U.remove(e, "fxshow"), f) C.style(e, i, f[i])
            })), l = pt(m ? v[i] : 0, i, p), i in v || (v[i] = l.start, m && (l.end = l.start, l.start = 0))
        }], prefilter: function (e, t) {
            t ? ft.prefilters.unshift(e) : ft.prefilters.push(e)
        }
    }), C.speed = function (e, t, n) {
        var i = e && "object" == _typeof(e) ? C.extend({}, e) : {
            complete: n || !n && t || C.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !C.isFunction(t) && t
        };
        return C.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in C.fx.speeds ? i.duration = C.fx.speeds[i.duration] : i.duration = C.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function () {
            C.isFunction(i.old) && i.old.call(this), i.queue && C.dequeue(this, i.queue)
        }, i
    }, C.fn.extend({
        fadeTo: function (e, t, n, i) {
            return this.filter(te).css("opacity", 0).show().end().animate({opacity: t}, e, n, i)
        }, animate: function (t, e, n, i) {
            function o() {
                var e = ft(this, C.extend({}, t), s);
                (r || U.get(this, "finish")) && e.stop(!0)
            }

            var r = C.isEmptyObject(t), s = C.speed(e, n, i);
            return o.finish = o, r || !1 === s.queue ? this.each(o) : this.queue(s.queue, o)
        }, stop: function (o, e, r) {
            function s(e) {
                var t = e.stop;
                delete e.stop, t(r)
            }

            return "string" != typeof o && (r = e, e = o, o = void 0), e && !1 !== o && this.queue(o || "fx", []), this.each(function () {
                var e = !0, t = null != o && o + "queueHooks", n = C.timers, i = U.get(this);
                if (t) i[t] && i[t].stop && s(i[t]); else for (t in i) i[t] && i[t].stop && lt.test(t) && s(i[t]);
                for (t = n.length; t--;) n[t].elem !== this || null != o && n[t].queue !== o || (n[t].anim.stop(r), e = !1, n.splice(t, 1));
                !e && r || C.dequeue(this, o)
            })
        }, finish: function (s) {
            return !1 !== s && (s = s || "fx"), this.each(function () {
                var e, t = U.get(this), n = t[s + "queue"], i = t[s + "queueHooks"], o = C.timers, r = n ? n.length : 0;
                for (t.finish = !0, C.queue(this, s, []), i && i.stop && i.stop.call(this, !0), e = o.length; e--;) o[e].elem === this && o[e].queue === s && (o[e].anim.stop(!0), o.splice(e, 1));
                for (e = 0; e < r; e++) n[e] && n[e].finish && n[e].finish.call(this);
                delete t.finish
            })
        }
    }), C.each(["toggle", "show", "hide"], function (e, i) {
        var o = C.fn[i];
        C.fn[i] = function (e, t, n) {
            return null == e || "boolean" == typeof e ? o.apply(this, arguments) : this.animate(dt(i, !0), e, t, n)
        }
    }), C.each({
        slideDown: dt("show"),
        slideUp: dt("hide"),
        slideToggle: dt("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (e, i) {
        C.fn[e] = function (e, t, n) {
            return this.animate(i, e, t, n)
        }
    }), C.timers = [], C.fx.tick = function () {
        var e, t = 0, n = C.timers;
        for (it = C.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
        n.length || C.fx.stop(), it = void 0
    }, C.fx.timer = function (e) {
        C.timers.push(e), C.fx.start()
    }, C.fx.interval = 13, C.fx.start = function () {
        ot || (ot = !0, ct())
    }, C.fx.stop = function () {
        ot = null
    }, C.fx.speeds = {slow: 600, fast: 200, _default: 400}, C.fn.delay = function (i, e) {
        return i = C.fx && C.fx.speeds[i] || i, e = e || "fx", this.queue(e, function (e, t) {
            var n = k.setTimeout(e, i);
            t.stop = function () {
                k.clearTimeout(n)
            }
        })
    }, rt = S.createElement("input"), st = S.createElement("select").appendChild(S.createElement("option")), rt.type = "checkbox", v.checkOn = "" !== rt.value, v.optSelected = st.selected, (rt = S.createElement("input")).value = "t", rt.type = "radio", v.radioValue = "t" === rt.value;
    var ht, mt = C.expr.attrHandle;
    C.fn.extend({
        attr: function (e, t) {
            return _(this, C.attr, e, t, 1 < arguments.length)
        }, removeAttr: function (e) {
            return this.each(function () {
                C.removeAttr(this, e)
            })
        }
    }), C.extend({
        attr: function (e, t, n) {
            var i, o, r = e.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return void 0 === e.getAttribute ? C.prop(e, t, n) : (1 === r && C.isXMLDoc(e) || (o = C.attrHooks[t.toLowerCase()] || (C.expr.match.bool.test(t) ? ht : void 0)), void 0 !== n ? null === n ? void C.removeAttr(e, t) : o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : o && "get" in o && null !== (i = o.get(e, t)) ? i : null == (i = C.find.attr(e, t)) ? void 0 : i)
        }, attrHooks: {
            type: {
                set: function (e, t) {
                    if (!v.radioValue && "radio" === t && $(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        }, removeAttr: function (e, t) {
            var n, i = 0, o = t && t.match(M);
            if (o && 1 === e.nodeType) for (; n = o[i++];) e.removeAttribute(n)
        }
    }), ht = {
        set: function (e, t, n) {
            return !1 === t ? C.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, C.each(C.expr.match.bool.source.match(/\w+/g), function (e, t) {
        var s = mt[t] || C.find.attr;
        mt[t] = function (e, t, n) {
            var i, o, r = t.toLowerCase();
            return n || (o = mt[r], mt[r] = i, i = null != s(e, t, n) ? r : null, mt[r] = o), i
        }
    });
    var vt = /^(?:input|select|textarea|button)$/i, gt = /^(?:a|area)$/i;

    function yt(e) {
        return (e.match(M) || []).join(" ")
    }

    function bt(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    C.fn.extend({
        prop: function (e, t) {
            return _(this, C.prop, e, t, 1 < arguments.length)
        }, removeProp: function (e) {
            return this.each(function () {
                delete this[C.propFix[e] || e]
            })
        }
    }), C.extend({
        prop: function (e, t, n) {
            var i, o, r = e.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return 1 === r && C.isXMLDoc(e) || (t = C.propFix[t] || t, o = C.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]
        }, propHooks: {
            tabIndex: {
                get: function (e) {
                    var t = C.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : vt.test(e.nodeName) || gt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }, propFix: {for: "htmlFor", class: "className"}
    }), v.optSelected || (C.propHooks.selected = {
        get: function (e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        }, set: function (e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), C.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        C.propFix[this.toLowerCase()] = this
    }), C.fn.extend({
        addClass: function (t) {
            var e, n, i, o, r, s, a, l = 0;
            if (C.isFunction(t)) return this.each(function (e) {
                C(this).addClass(t.call(this, e, bt(this)))
            });
            if ("string" == typeof t && t) for (e = t.match(M) || []; n = this[l++];) if (o = bt(n), i = 1 === n.nodeType && " " + yt(o) + " ") {
                for (s = 0; r = e[s++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                o !== (a = yt(i)) && n.setAttribute("class", a)
            }
            return this
        }, removeClass: function (t) {
            var e, n, i, o, r, s, a, l = 0;
            if (C.isFunction(t)) return this.each(function (e) {
                C(this).removeClass(t.call(this, e, bt(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof t && t) for (e = t.match(M) || []; n = this[l++];) if (o = bt(n), i = 1 === n.nodeType && " " + yt(o) + " ") {
                for (s = 0; r = e[s++];) for (; -1 < i.indexOf(" " + r + " ");) i = i.replace(" " + r + " ", " ");
                o !== (a = yt(i)) && n.setAttribute("class", a)
            }
            return this
        }, toggleClass: function (o, t) {
            var r = _typeof(o);
            return "boolean" == typeof t && "string" === r ? t ? this.addClass(o) : this.removeClass(o) : C.isFunction(o) ? this.each(function (e) {
                C(this).toggleClass(o.call(this, e, bt(this), t), t)
            }) : this.each(function () {
                var e, t, n, i;
                if ("string" === r) for (t = 0, n = C(this), i = o.match(M) || []; e = i[t++];) n.hasClass(e) ? n.removeClass(e) : n.addClass(e); else void 0 !== o && "boolean" !== r || ((e = bt(this)) && U.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === o ? "" : U.get(this, "__className__") || ""))
            })
        }, hasClass: function (e) {
            var t, n, i = 0;
            for (t = " " + e + " "; n = this[i++];) if (1 === n.nodeType && -1 < (" " + yt(bt(n)) + " ").indexOf(t)) return !0;
            return !1
        }
    });
    var wt = /\r/g;
    C.fn.extend({
        val: function (n) {
            var i, e, o, t = this[0];
            return arguments.length ? (o = C.isFunction(n), this.each(function (e) {
                var t;
                1 === this.nodeType && (null == (t = o ? n.call(this, e, C(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = C.map(t, function (e) {
                    return null == e ? "" : e + ""
                })), (i = C.valHooks[this.type] || C.valHooks[this.nodeName.toLowerCase()]) && "set" in i && void 0 !== i.set(this, t, "value") || (this.value = t))
            })) : t ? (i = C.valHooks[t.type] || C.valHooks[t.nodeName.toLowerCase()]) && "get" in i && void 0 !== (e = i.get(t, "value")) ? e : "string" == typeof (e = t.value) ? e.replace(wt, "") : null == e ? "" : e : void 0
        }
    }), C.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = C.find.attr(e, "value");
                    return null != t ? t : yt(C.text(e))
                }
            }, select: {
                get: function (e) {
                    var t, n, i, o = e.options, r = e.selectedIndex, s = "select-one" === e.type, a = s ? null : [],
                        l = s ? r + 1 : o.length;
                    for (i = r < 0 ? l : s ? r : 0; i < l; i++) if (((n = o[i]).selected || i === r) && !n.disabled && (!n.parentNode.disabled || !$(n.parentNode, "optgroup"))) {
                        if (t = C(n).val(), s) return t;
                        a.push(t)
                    }
                    return a
                }, set: function (e, t) {
                    for (var n, i, o = e.options, r = C.makeArray(t), s = o.length; s--;) ((i = o[s]).selected = -1 < C.inArray(C.valHooks.option.get(i), r)) && (n = !0);
                    return n || (e.selectedIndex = -1), r
                }
            }
        }
    }), C.each(["radio", "checkbox"], function () {
        C.valHooks[this] = {
            set: function (e, t) {
                if (Array.isArray(t)) return e.checked = -1 < C.inArray(C(e).val(), t)
            }
        }, v.checkOn || (C.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var xt = /^(?:focusinfocus|focusoutblur)$/;
    C.extend(C.event, {
        trigger: function (e, t, n, i) {
            var o, r, s, a, l, c, u, d = [n || S], p = h.call(e, "type") ? e.type : e,
                f = h.call(e, "namespace") ? e.namespace.split(".") : [];
            if (r = s = n = n || S, 3 !== n.nodeType && 8 !== n.nodeType && !xt.test(p + C.event.triggered) && (-1 < p.indexOf(".") && (p = (f = p.split(".")).shift(), f.sort()), l = p.indexOf(":") < 0 && "on" + p, (e = e[C.expando] ? e : new C.Event(p, "object" == _typeof(e) && e)).isTrigger = i ? 2 : 3, e.namespace = f.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : C.makeArray(t, [e]), u = C.event.special[p] || {}, i || !u.trigger || !1 !== u.trigger.apply(n, t))) {
                if (!i && !u.noBubble && !C.isWindow(n)) {
                    for (a = u.delegateType || p, xt.test(a + p) || (r = r.parentNode); r; r = r.parentNode) d.push(r), s = r;
                    s === (n.ownerDocument || S) && d.push(s.defaultView || s.parentWindow || k)
                }
                for (o = 0; (r = d[o++]) && !e.isPropagationStopped();) e.type = 1 < o ? a : u.bindType || p, (c = (U.get(r, "events") || {})[e.type] && U.get(r, "handle")) && c.apply(r, t), (c = l && r[l]) && c.apply && B(r) && (e.result = c.apply(r, t), !1 === e.result && e.preventDefault());
                return e.type = p, i || e.isDefaultPrevented() || u._default && !1 !== u._default.apply(d.pop(), t) || !B(n) || l && C.isFunction(n[p]) && !C.isWindow(n) && ((s = n[l]) && (n[l] = null), n[C.event.triggered = p](), C.event.triggered = void 0, s && (n[l] = s)), e.result
            }
        }, simulate: function (e, t, n) {
            var i = C.extend(new C.Event, n, {type: e, isSimulated: !0});
            C.event.trigger(i, null, t)
        }
    }), C.fn.extend({
        trigger: function (e, t) {
            return this.each(function () {
                C.event.trigger(e, t, this)
            })
        }, triggerHandler: function (e, t) {
            var n = this[0];
            if (n) return C.event.trigger(e, t, n, !0)
        }
    }), C.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, n) {
        C.fn[n] = function (e, t) {
            return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
        }
    }), C.fn.extend({
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), v.focusin = "onfocusin" in k, v.focusin || C.each({focus: "focusin", blur: "focusout"}, function (n, i) {
        function o(e) {
            C.event.simulate(i, e.target, C.event.fix(e))
        }

        C.event.special[i] = {
            setup: function () {
                var e = this.ownerDocument || this, t = U.access(e, i);
                t || e.addEventListener(n, o, !0), U.access(e, i, (t || 0) + 1)
            }, teardown: function () {
                var e = this.ownerDocument || this, t = U.access(e, i) - 1;
                t ? U.access(e, i, t) : (e.removeEventListener(n, o, !0), U.remove(e, i))
            }
        }
    });
    var Tt = k.location, kt = C.now(), St = /\?/;
    C.parseXML = function (e) {
        var t;
        if (!e || "string" != typeof e) return null;
        try {
            t = (new k.DOMParser).parseFromString(e, "text/xml")
        } catch (e) {
            t = void 0
        }
        return t && !t.getElementsByTagName("parsererror").length || C.error("Invalid XML: " + e), t
    };
    var Ct = /\[\]$/, At = /\r?\n/g, $t = /^(?:submit|button|image|reset|file)$/i,
        Et = /^(?:input|select|textarea|keygen)/i;

    function jt(n, e, i, o) {
        var t;
        if (Array.isArray(e)) C.each(e, function (e, t) {
            i || Ct.test(n) ? o(n, t) : jt(n + "[" + ("object" == _typeof(t) && null != t ? e : "") + "]", t, i, o)
        }); else if (i || "object" !== C.type(e)) o(n, e); else for (t in e) jt(n + "[" + t + "]", e[t], i, o)
    }

    C.param = function (e, t) {
        function n(e, t) {
            var n = C.isFunction(t) ? t() : t;
            o[o.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
        }

        var i, o = [];
        if (Array.isArray(e) || e.jquery && !C.isPlainObject(e)) C.each(e, function () {
            n(this.name, this.value)
        }); else for (i in e) jt(i, e[i], t, n);
        return o.join("&")
    }, C.fn.extend({
        serialize: function () {
            return C.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var e = C.prop(this, "elements");
                return e ? C.makeArray(e) : this
            }).filter(function () {
                var e = this.type;
                return this.name && !C(this).is(":disabled") && Et.test(this.nodeName) && !$t.test(e) && (this.checked || !re.test(e))
            }).map(function (e, t) {
                var n = C(this).val();
                return null == n ? null : Array.isArray(n) ? C.map(n, function (e) {
                    return {name: t.name, value: e.replace(At, "\r\n")}
                }) : {name: t.name, value: n.replace(At, "\r\n")}
            }).get()
        }
    });
    var Lt = /%20/g, Dt = /#.*$/, Nt = /([?&])_=[^&]*/, Ot = /^(.*?):[ \t]*([^\r\n]*)$/gm, qt = /^(?:GET|HEAD)$/,
        Ht = /^\/\//, Mt = {}, Pt = {}, It = "*/".concat("*"), Ft = S.createElement("a");

    function zt(r) {
        return function (e, t) {
            "string" != typeof e && (t = e, e = "*");
            var n, i = 0, o = e.toLowerCase().match(M) || [];
            if (C.isFunction(t)) for (; n = o[i++];) "+" === n[0] ? (n = n.slice(1) || "*", (r[n] = r[n] || []).unshift(t)) : (r[n] = r[n] || []).push(t)
        }
    }

    function Rt(t, o, r, s) {
        var a = {}, l = t === Pt;

        function c(e) {
            var i;
            return a[e] = !0, C.each(t[e] || [], function (e, t) {
                var n = t(o, r, s);
                return "string" != typeof n || l || a[n] ? l ? !(i = n) : void 0 : (o.dataTypes.unshift(n), c(n), !1)
            }), i
        }

        return c(o.dataTypes[0]) || !a["*"] && c("*")
    }

    function Wt(e, t) {
        var n, i, o = C.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((o[n] ? e : i || (i = {}))[n] = t[n]);
        return i && C.extend(!0, e, i), e
    }

    Ft.href = Tt.href, C.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Tt.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Tt.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": It,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": C.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (e, t) {
            return t ? Wt(Wt(e, C.ajaxSettings), t) : Wt(C.ajaxSettings, e)
        },
        ajaxPrefilter: zt(Mt),
        ajaxTransport: zt(Pt),
        ajax: function (e, t) {
            "object" == _typeof(e) && (t = e, e = void 0), t = t || {};
            var u, d, p, n, f, i, h, m, o, r, v = C.ajaxSetup({}, t), g = v.context || v,
                y = v.context && (g.nodeType || g.jquery) ? C(g) : C.event, b = C.Deferred(),
                w = C.Callbacks("once memory"), x = v.statusCode || {}, s = {}, a = {}, l = "canceled", T = {
                    readyState: 0, getResponseHeader: function (e) {
                        var t;
                        if (h) {
                            if (!n) for (n = {}; t = Ot.exec(p);) n[t[1].toLowerCase()] = t[2];
                            t = n[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    }, getAllResponseHeaders: function () {
                        return h ? p : null
                    }, setRequestHeader: function (e, t) {
                        return null == h && (e = a[e.toLowerCase()] = a[e.toLowerCase()] || e, s[e] = t), this
                    }, overrideMimeType: function (e) {
                        return null == h && (v.mimeType = e), this
                    }, statusCode: function (e) {
                        var t;
                        if (e) if (h) T.always(e[T.status]); else for (t in e) x[t] = [x[t], e[t]];
                        return this
                    }, abort: function (e) {
                        var t = e || l;
                        return u && u.abort(t), c(0, t), this
                    }
                };
            if (b.promise(T), v.url = ((e || v.url || Tt.href) + "").replace(Ht, Tt.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(M) || [""], null == v.crossDomain) {
                i = S.createElement("a");
                try {
                    i.href = v.url, i.href = i.href, v.crossDomain = Ft.protocol + "//" + Ft.host != i.protocol + "//" + i.host
                } catch (e) {
                    v.crossDomain = !0
                }
            }
            if (v.data && v.processData && "string" != typeof v.data && (v.data = C.param(v.data, v.traditional)), Rt(Mt, v, t, T), h) return T;
            for (o in (m = C.event && v.global) && 0 == C.active++ && C.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !qt.test(v.type), d = v.url.replace(Dt, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(Lt, "+")) : (r = v.url.slice(d.length), v.data && (d += (St.test(d) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (d = d.replace(Nt, "$1"), r = (St.test(d) ? "&" : "?") + "_=" + kt++ + r), v.url = d + r), v.ifModified && (C.lastModified[d] && T.setRequestHeader("If-Modified-Since", C.lastModified[d]), C.etag[d] && T.setRequestHeader("If-None-Match", C.etag[d])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && T.setRequestHeader("Content-Type", v.contentType), T.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + It + "; q=0.01" : "") : v.accepts["*"]), v.headers) T.setRequestHeader(o, v.headers[o]);
            if (v.beforeSend && (!1 === v.beforeSend.call(g, T, v) || h)) return T.abort();
            if (l = "abort", w.add(v.complete), T.done(v.success), T.fail(v.error), u = Rt(Pt, v, t, T)) {
                if (T.readyState = 1, m && y.trigger("ajaxSend", [T, v]), h) return T;
                v.async && 0 < v.timeout && (f = k.setTimeout(function () {
                    T.abort("timeout")
                }, v.timeout));
                try {
                    h = !1, u.send(s, c)
                } catch (e) {
                    if (h) throw e;
                    c(-1, e)
                }
            } else c(-1, "No Transport");

            function c(e, t, n, i) {
                var o, r, s, a, l, c = t;
                h || (h = !0, f && k.clearTimeout(f), u = void 0, p = i || "", T.readyState = 0 < e ? 4 : 0, o = 200 <= e && e < 300 || 304 === e, n && (a = function (e, t, n) {
                    for (var i, o, r, s, a = e.contents, l = e.dataTypes; "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
                    if (i) for (o in a) if (a[o] && a[o].test(i)) {
                        l.unshift(o);
                        break
                    }
                    if (l[0] in n) r = l[0]; else {
                        for (o in n) {
                            if (!l[0] || e.converters[o + " " + l[0]]) {
                                r = o;
                                break
                            }
                            s || (s = o)
                        }
                        r = r || s
                    }
                    if (r) return r !== l[0] && l.unshift(r), n[r]
                }(v, T, n)), a = function (e, t, n, i) {
                    var o, r, s, a, l, c = {}, u = e.dataTypes.slice();
                    if (u[1]) for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
                    for (r = u.shift(); r;) if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift()) if ("*" === r) r = l; else if ("*" !== l && l !== r) {
                        if (!(s = c[l + " " + r] || c["* " + r])) for (o in c) if ((a = o.split(" "))[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                            !0 === s ? s = c[o] : !0 !== c[o] && (r = a[0], u.unshift(a[1]));
                            break
                        }
                        if (!0 !== s) if (s && e.throws) t = s(t); else try {
                            t = s(t)
                        } catch (e) {
                            return {state: "parsererror", error: s ? e : "No conversion from " + l + " to " + r}
                        }
                    }
                    return {state: "success", data: t}
                }(v, a, T, o), o ? (v.ifModified && ((l = T.getResponseHeader("Last-Modified")) && (C.lastModified[d] = l), (l = T.getResponseHeader("etag")) && (C.etag[d] = l)), 204 === e || "HEAD" === v.type ? c = "nocontent" : 304 === e ? c = "notmodified" : (c = a.state, r = a.data, o = !(s = a.error))) : (s = c, !e && c || (c = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (t || c) + "", o ? b.resolveWith(g, [r, c, T]) : b.rejectWith(g, [T, c, s]), T.statusCode(x), x = void 0, m && y.trigger(o ? "ajaxSuccess" : "ajaxError", [T, v, o ? r : s]), w.fireWith(g, [T, c]), m && (y.trigger("ajaxComplete", [T, v]), --C.active || C.event.trigger("ajaxStop")))
            }

            return T
        },
        getJSON: function (e, t, n) {
            return C.get(e, t, n, "json")
        },
        getScript: function (e, t) {
            return C.get(e, void 0, t, "script")
        }
    }), C.each(["get", "post"], function (e, o) {
        C[o] = function (e, t, n, i) {
            return C.isFunction(t) && (i = i || n, n = t, t = void 0), C.ajax(C.extend({
                url: e,
                type: o,
                dataType: i,
                data: t,
                success: n
            }, C.isPlainObject(e) && e))
        }
    }), C._evalUrl = function (e) {
        return C.ajax({url: e, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0})
    }, C.fn.extend({
        wrapAll: function (e) {
            var t;
            return this[0] && (C.isFunction(e) && (e = e.call(this[0])), t = C(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e
            }).append(this)), this
        }, wrapInner: function (n) {
            return C.isFunction(n) ? this.each(function (e) {
                C(this).wrapInner(n.call(this, e))
            }) : this.each(function () {
                var e = C(this), t = e.contents();
                t.length ? t.wrapAll(n) : e.append(n)
            })
        }, wrap: function (t) {
            var n = C.isFunction(t);
            return this.each(function (e) {
                C(this).wrapAll(n ? t.call(this, e) : t)
            })
        }, unwrap: function (e) {
            return this.parent(e).not("body").each(function () {
                C(this).replaceWith(this.childNodes)
            }), this
        }
    }), C.expr.pseudos.hidden = function (e) {
        return !C.expr.pseudos.visible(e)
    }, C.expr.pseudos.visible = function (e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, C.ajaxSettings.xhr = function () {
        try {
            return new k.XMLHttpRequest
        } catch (e) {
        }
    };
    var _t = {0: 200, 1223: 204}, Bt = C.ajaxSettings.xhr();
    v.cors = !!Bt && "withCredentials" in Bt, v.ajax = Bt = !!Bt, C.ajaxTransport(function (o) {
        var r, s;
        if (v.cors || Bt && !o.crossDomain) return {
            send: function (e, t) {
                var n, i = o.xhr();
                if (i.open(o.type, o.url, o.async, o.username, o.password), o.xhrFields) for (n in o.xhrFields) i[n] = o.xhrFields[n];
                for (n in o.mimeType && i.overrideMimeType && i.overrideMimeType(o.mimeType), o.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) i.setRequestHeader(n, e[n]);
                r = function (e) {
                    return function () {
                        r && (r = s = i.onload = i.onerror = i.onabort = i.onreadystatechange = null, "abort" === e ? i.abort() : "error" === e ? "number" != typeof i.status ? t(0, "error") : t(i.status, i.statusText) : t(_t[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {binary: i.response} : {text: i.responseText}, i.getAllResponseHeaders()))
                    }
                }, i.onload = r(), s = i.onerror = r("error"), void 0 !== i.onabort ? i.onabort = s : i.onreadystatechange = function () {
                    4 === i.readyState && k.setTimeout(function () {
                        r && s()
                    })
                }, r = r("abort");
                try {
                    i.send(o.hasContent && o.data || null)
                } catch (e) {
                    if (r) throw e
                }
            }, abort: function () {
                r && r()
            }
        }
    }), C.ajaxPrefilter(function (e) {
        e.crossDomain && (e.contents.script = !1)
    }), C.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /\b(?:java|ecma)script\b/},
        converters: {
            "text script": function (e) {
                return C.globalEval(e), e
            }
        }
    }), C.ajaxPrefilter("script", function (e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), C.ajaxTransport("script", function (n) {
        var i, o;
        if (n.crossDomain) return {
            send: function (e, t) {
                i = C("<script>").prop({charset: n.scriptCharset, src: n.url}).on("load error", o = function (e) {
                    i.remove(), o = null, e && t("error" === e.type ? 404 : 200, e.type)
                }), S.head.appendChild(i[0])
            }, abort: function () {
                o && o()
            }
        }
    });
    var Xt, Ut = [], Yt = /(=)\?(?=&|$)|\?\?/;
    C.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var e = Ut.pop() || C.expando + "_" + kt++;
            return this[e] = !0, e
        }
    }), C.ajaxPrefilter("json jsonp", function (e, t, n) {
        var i, o, r,
            s = !1 !== e.jsonp && (Yt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Yt.test(e.data) && "data");
        if (s || "jsonp" === e.dataTypes[0]) return i = e.jsonpCallback = C.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(Yt, "$1" + i) : !1 !== e.jsonp && (e.url += (St.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), e.converters["script json"] = function () {
            return r || C.error(i + " was not called"), r[0]
        }, e.dataTypes[0] = "json", o = k[i], k[i] = function () {
            r = arguments
        }, n.always(function () {
            void 0 === o ? C(k).removeProp(i) : k[i] = o, e[i] && (e.jsonpCallback = t.jsonpCallback, Ut.push(i)), r && C.isFunction(o) && o(r[0]), r = o = void 0
        }), "script"
    }), v.createHTMLDocument = ((Xt = S.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Xt.childNodes.length), C.parseHTML = function (e, t, n) {
        return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (v.createHTMLDocument ? ((i = (t = S.implementation.createHTMLDocument("")).createElement("base")).href = S.location.href, t.head.appendChild(i)) : t = S), r = !n && [], (o = E.exec(e)) ? [t.createElement(o[1])] : (o = he([e], t, r), r && r.length && C(r).remove(), C.merge([], o.childNodes)));
        var i, o, r
    }, C.fn.load = function (e, t, n) {
        var i, o, r, s = this, a = e.indexOf(" ");
        return -1 < a && (i = yt(e.slice(a)), e = e.slice(0, a)), C.isFunction(t) ? (n = t, t = void 0) : t && "object" == _typeof(t) && (o = "POST"), 0 < s.length && C.ajax({
            url: e,
            type: o || "GET",
            dataType: "html",
            data: t
        }).done(function (e) {
            r = arguments, s.html(i ? C("<div>").append(C.parseHTML(e)).find(i) : e)
        }).always(n && function (e, t) {
            s.each(function () {
                n.apply(this, r || [e.responseText, t, e])
            })
        }), this
    }, C.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
        C.fn[t] = function (e) {
            return this.on(t, e)
        }
    }), C.expr.pseudos.animated = function (t) {
        return C.grep(C.timers, function (e) {
            return t === e.elem
        }).length
    }, C.offset = {
        setOffset: function (e, t, n) {
            var i, o, r, s, a, l, c = C.css(e, "position"), u = C(e), d = {};
            "static" === c && (e.style.position = "relative"), a = u.offset(), r = C.css(e, "top"), l = C.css(e, "left"), o = ("absolute" === c || "fixed" === c) && -1 < (r + l).indexOf("auto") ? (s = (i = u.position()).top, i.left) : (s = parseFloat(r) || 0, parseFloat(l) || 0), C.isFunction(t) && (t = t.call(e, n, C.extend({}, a))), null != t.top && (d.top = t.top - a.top + s), null != t.left && (d.left = t.left - a.left + o), "using" in t ? t.using.call(e, d) : u.css(d)
        }
    }, C.fn.extend({
        offset: function (t) {
            if (arguments.length) return void 0 === t ? this : this.each(function (e) {
                C.offset.setOffset(this, t, e)
            });
            var e, n, i, o, r = this[0];
            return r ? r.getClientRects().length ? (i = r.getBoundingClientRect(), n = (e = r.ownerDocument).documentElement, o = e.defaultView, {
                top: i.top + o.pageYOffset - n.clientTop,
                left: i.left + o.pageXOffset - n.clientLeft
            }) : {top: 0, left: 0} : void 0
        }, position: function () {
            if (this[0]) {
                var e, t, n = this[0], i = {top: 0, left: 0};
                return "fixed" === C.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), $(e[0], "html") || (i = e.offset()), i = {
                    top: i.top + C.css(e[0], "borderTopWidth", !0),
                    left: i.left + C.css(e[0], "borderLeftWidth", !0)
                }), {top: t.top - i.top - C.css(n, "marginTop", !0), left: t.left - i.left - C.css(n, "marginLeft", !0)}
            }
        }, offsetParent: function () {
            return this.map(function () {
                for (var e = this.offsetParent; e && "static" === C.css(e, "position");) e = e.offsetParent;
                return e || me
            })
        }
    }), C.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (t, o) {
        var r = "pageYOffset" === o;
        C.fn[t] = function (e) {
            return _(this, function (e, t, n) {
                var i;
                return C.isWindow(e) ? i = e : 9 === e.nodeType && (i = e.defaultView), void 0 === n ? i ? i[o] : e[t] : void (i ? i.scrollTo(r ? i.pageXOffset : n, r ? n : i.pageYOffset) : e[t] = n)
            }, t, e, arguments.length)
        }
    }), C.each(["top", "left"], function (e, n) {
        C.cssHooks[n] = Xe(v.pixelPosition, function (e, t) {
            if (t) return t = Be(e, n), Re.test(t) ? C(e).position()[n] + "px" : t
        })
    }), C.each({Height: "height", Width: "width"}, function (s, a) {
        C.each({padding: "inner" + s, content: a, "": "outer" + s}, function (i, r) {
            C.fn[r] = function (e, t) {
                var n = arguments.length && (i || "boolean" != typeof e),
                    o = i || (!0 === e || !0 === t ? "margin" : "border");
                return _(this, function (e, t, n) {
                    var i;
                    return C.isWindow(e) ? 0 === r.indexOf("outer") ? e["inner" + s] : e.document.documentElement["client" + s] : 9 === e.nodeType ? (i = e.documentElement, Math.max(e.body["scroll" + s], i["scroll" + s], e.body["offset" + s], i["offset" + s], i["client" + s])) : void 0 === n ? C.css(e, t, o) : C.style(e, t, n, o)
                }, a, n ? e : void 0, n)
            }
        })
    }), C.fn.extend({
        bind: function (e, t, n) {
            return this.on(e, null, t, n)
        }, unbind: function (e, t) {
            return this.off(e, null, t)
        }, delegate: function (e, t, n, i) {
            return this.on(t, e, n, i)
        }, undelegate: function (e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    }), C.holdReady = function (e) {
        e ? C.readyWait++ : C.ready(!0)
    }, C.isArray = Array.isArray, C.parseJSON = JSON.parse, C.nodeName = $, "function" == typeof define && define.amd && define("jquery", [], function () {
        return C
    });
    var Vt = k.jQuery, Gt = k.$;
    return C.noConflict = function (e) {
        return k.$ === C && (k.$ = Gt), e && k.jQuery === C && (k.jQuery = Vt), C
    }, e || (k.jQuery = k.$ = C), C
}), function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("undefined" != typeof jQuery ? jQuery : window.Zepto)
}(function (O) {
    function n(e) {
        var t = e.data;
        e.isDefaultPrevented() || (e.preventDefault(), O(e.target).ajaxSubmit(t))
    }

    function i(e) {
        var t = e.target, n = O(t);
        if (!n.is("[type=submit],[type=image]")) {
            var i = n.closest("[type=submit]");
            if (0 === i.length) return;
            t = i[0]
        }
        var o = this;
        if ("image" == (o.clk = t).type) if (void 0 !== e.offsetX) o.clk_x = e.offsetX, o.clk_y = e.offsetY; else if ("function" == typeof O.fn.offset) {
            var r = n.offset();
            o.clk_x = e.pageX - r.left, o.clk_y = e.pageY - r.top
        } else o.clk_x = e.pageX - t.offsetLeft, o.clk_y = e.pageY - t.offsetTop;
        setTimeout(function () {
            o.clk = o.clk_x = o.clk_y = null
        }, 100)
    }

    function q() {
        if (O.fn.ajaxSubmit.debug) {
            var e = "[jquery.form] " + Array.prototype.join.call(arguments, "");
            window.console && window.console.log ? window.console.log(e) : window.opera && window.opera.postError && window.opera.postError(e)
        }
    }

    var b = {};
    b.fileapi = void 0 !== O("<input type='file'/>").get(0).files, b.formdata = void 0 !== window.FormData;
    var H = !!O.fn.prop;
    O.fn.attr2 = function () {
        if (!H) return this.attr.apply(this, arguments);
        var e = this.prop.apply(this, arguments);
        return e && e.jquery || "string" == typeof e ? e : this.attr.apply(this, arguments)
    }, O.fn.ajaxSubmit = function (j) {
        function e(e) {
            function u(t) {
                var n = null;
                try {
                    t.contentWindow && (n = t.contentWindow.document)
                } catch (e) {
                    q("cannot get iframe.contentWindow document: " + e)
                }
                if (n) return n;
                try {
                    n = t.contentDocument ? t.contentDocument : t.document
                } catch (e) {
                    q("cannot get iframe.contentDocument: " + e), n = t.document
                }
                return n
            }

            function t() {
                var e = D.attr2("target"), t = D.attr2("action"),
                    n = D.attr("enctype") || D.attr("encoding") || "multipart/form-data";
                a.setAttribute("target", r), L && !/post/i.test(L) || a.setAttribute("method", "POST"), t != p.url && a.setAttribute("action", p.url), p.skipEncodingOverride || L && !/post/i.test(L) || D.attr({
                    encoding: "multipart/form-data",
                    enctype: "multipart/form-data"
                }), p.timeout && (y = setTimeout(function () {
                    g = !0, d(w)
                }, p.timeout));
                var i = [];
                try {
                    if (p.extraData) for (var o in p.extraData) p.extraData.hasOwnProperty(o) && i.push(O.isPlainObject(p.extraData[o]) && p.extraData[o].hasOwnProperty("name") && p.extraData[o].hasOwnProperty("value") ? O('<input type="hidden" name="' + p.extraData[o].name + '">').val(p.extraData[o].value).appendTo(a)[0] : O('<input type="hidden" name="' + o + '">').val(p.extraData[o]).appendTo(a)[0]);
                    p.iframeTarget || h.appendTo("body"), m.attachEvent ? m.attachEvent("onload", d) : m.addEventListener("load", d, !1), setTimeout(function e() {
                        try {
                            var t = u(m).readyState;
                            q("state = " + t), t && "uninitialized" == t.toLowerCase() && setTimeout(e, 50)
                        } catch (e) {
                            q("Server abort: ", e, " (", e.name, ")"), d(x), y && clearTimeout(y), y = void 0
                        }
                    }, 15);
                    try {
                        a.submit()
                    } catch (e) {
                        document.createElement("form").submit.apply(a)
                    }
                } finally {
                    a.setAttribute("action", t), a.setAttribute("enctype", n), e ? a.setAttribute("target", e) : D.removeAttr("target"), O(i).remove()
                }
            }

            function d(e) {
                if (!v.aborted && !S) {
                    if ((k = u(m)) || (q("cannot access response document"), e = x), e === w && v) return v.abort("timeout"), void b.reject(v, "timeout");
                    if (e == x && v) return v.abort("server abort"), void b.reject(v, "error", "server abort");
                    if (k && k.location.href != p.iframeSrc || g) {
                        m.detachEvent ? m.detachEvent("onload", d) : m.removeEventListener("load", d, !1);
                        var t, n = "success";
                        try {
                            if (g) throw"timeout";
                            var i = "xml" == p.dataType || k.XMLDocument || O.isXMLDoc(k);
                            if (q("isXml=" + i), !i && window.opera && (null === k.body || !k.body.innerHTML) && --C) return q("requeing onLoad callback, DOM not available"), void setTimeout(d, 250);
                            var o = k.body ? k.body : k.documentElement;
                            v.responseText = o ? o.innerHTML : null, v.responseXML = k.XMLDocument ? k.XMLDocument : k, i && (p.dataType = "xml"), v.getResponseHeader = function (e) {
                                return {"content-type": p.dataType}[e.toLowerCase()]
                            }, o && (v.status = Number(o.getAttribute("status")) || v.status, v.statusText = o.getAttribute("statusText") || v.statusText);
                            var r = (p.dataType || "").toLowerCase(), s = /(json|script|text)/.test(r);
                            if (s || p.textarea) {
                                var a = k.getElementsByTagName("textarea")[0];
                                if (a) v.responseText = a.value, v.status = Number(a.getAttribute("status")) || v.status, v.statusText = a.getAttribute("statusText") || v.statusText; else if (s) {
                                    var l = k.getElementsByTagName("pre")[0], c = k.getElementsByTagName("body")[0];
                                    l ? v.responseText = l.textContent ? l.textContent : l.innerText : c && (v.responseText = c.textContent ? c.textContent : c.innerText)
                                }
                            } else "xml" == r && !v.responseXML && v.responseText && (v.responseXML = A(v.responseText));
                            try {
                                T = E(v, r, p)
                            } catch (e) {
                                n = "parsererror", v.error = t = e || n
                            }
                        } catch (e) {
                            q("error caught: ", e), n = "error", v.error = t = e || n
                        }
                        v.aborted && (q("upload aborted"), n = null), v.status && (n = 200 <= v.status && v.status < 300 || 304 === v.status ? "success" : "error"), "success" === n ? (p.success && p.success.call(p.context, T, "success", v), b.resolve(v.responseText, "success", v), f && O.event.trigger("ajaxSuccess", [v, p])) : n && (void 0 === t && (t = v.statusText), p.error && p.error.call(p.context, v, n, t), b.reject(v, "error", t), f && O.event.trigger("ajaxError", [v, p, t])), f && O.event.trigger("ajaxComplete", [v, p]), f && !--O.active && O.event.trigger("ajaxStop"), p.complete && p.complete.call(p.context, v, n), S = !0, p.timeout && clearTimeout(y), setTimeout(function () {
                            p.iframeTarget ? h.attr("src", p.iframeSrc) : h.remove(), v.responseXML = null
                        }, 100)
                    }
                }
            }

            var n, i, p, f, r, h, m, v, o, s, g, y, a = D[0], b = O.Deferred();
            if (b.abort = function (e) {
                v.abort(e)
            }, e) for (i = 0; i < N.length; i++) n = O(N[i]), H ? n.prop("disabled", !1) : n.removeAttr("disabled");
            if ((p = O.extend(!0, {}, O.ajaxSettings, j)).context = p.context || p, r = "jqFormIO" + (new Date).getTime(), p.iframeTarget ? (s = (h = O(p.iframeTarget)).attr2("name")) ? r = s : h.attr2("name", r) : (h = O('<iframe name="' + r + '" src="' + p.iframeSrc + '" />')).css({
                position: "absolute",
                top: "-1000px",
                left: "-1000px"
            }), m = h[0], v = {
                aborted: 0,
                responseText: null,
                responseXML: null,
                status: 0,
                statusText: "n/a",
                getAllResponseHeaders: function () {
                },
                getResponseHeader: function () {
                },
                setRequestHeader: function () {
                },
                abort: function (e) {
                    var t = "timeout" === e ? "timeout" : "aborted";
                    q("aborting upload... " + t), this.aborted = 1;
                    try {
                        m.contentWindow.document.execCommand && m.contentWindow.document.execCommand("Stop")
                    } catch (e) {
                    }
                    h.attr("src", p.iframeSrc), v.error = t, p.error && p.error.call(p.context, v, t, e), f && O.event.trigger("ajaxError", [v, p, t]), p.complete && p.complete.call(p.context, v, t)
                }
            }, (f = p.global) && 0 == O.active++ && O.event.trigger("ajaxStart"), f && O.event.trigger("ajaxSend", [v, p]), p.beforeSend && !1 === p.beforeSend.call(p.context, v, p)) return p.global && O.active--, b.reject(), b;
            if (v.aborted) return b.reject(), b;
            !(o = a.clk) || (s = o.name) && !o.disabled && (p.extraData = p.extraData || {}, p.extraData[s] = o.value, "image" == o.type && (p.extraData[s + ".x"] = a.clk_x, p.extraData[s + ".y"] = a.clk_y));
            var w = 1, x = 2, l = O("meta[name=csrf-token]").attr("content"),
                c = O("meta[name=csrf-param]").attr("content");
            c && l && (p.extraData = p.extraData || {}, p.extraData[c] = l), p.forceSync ? t() : setTimeout(t, 10);
            var T, k, S, C = 50, A = O.parseXML || function (e, t) {
                return window.ActiveXObject ? ((t = new ActiveXObject("Microsoft.XMLDOM")).async = "false", t.loadXML(e)) : t = (new DOMParser).parseFromString(e, "text/xml"), t && t.documentElement && "parsererror" != t.documentElement.nodeName ? t : null
            }, $ = O.parseJSON || function (e) {
                return window.eval("(" + e + ")")
            }, E = function (e, t, n) {
                var i = e.getResponseHeader("content-type") || "", o = "xml" === t || !t && 0 <= i.indexOf("xml"),
                    r = o ? e.responseXML : e.responseText;
                return o && "parsererror" === r.documentElement.nodeName && O.error && O.error("parsererror"), n && n.dataFilter && (r = n.dataFilter(r, t)), "string" == typeof r && ("json" === t || !t && 0 <= i.indexOf("json") ? r = $(r) : ("script" === t || !t && 0 <= i.indexOf("javascript")) && O.globalEval(r)), r
            };
            return b
        }

        if (!this.length) return q("ajaxSubmit: skipping submit process - no element selected"), this;
        var L, t, n, D = this;
        "function" == typeof j ? j = {success: j} : void 0 === j && (j = {}), L = j.type || this.attr2("method"), (n = (n = "string" == typeof (t = j.url || this.attr2("action")) ? O.trim(t) : "") || window.location.href || "") && (n = (n.match(/^([^#]+)/) || [])[1]), j = O.extend(!0, {
            url: n,
            success: O.ajaxSettings.success,
            type: L || O.ajaxSettings.type,
            iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
        }, j);
        var i = {};
        if (this.trigger("form-pre-serialize", [this, j, i]), i.veto) return q("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this;
        if (j.beforeSerialize && !1 === j.beforeSerialize(this, j)) return q("ajaxSubmit: submit aborted via beforeSerialize callback"), this;
        var o = j.traditional;
        void 0 === o && (o = O.ajaxSettings.traditional);
        var r, N = [], s = this.formToArray(j.semantic, N);
        if (j.data && (j.extraData = j.data, r = O.param(j.data, o)), j.beforeSubmit && !1 === j.beforeSubmit(s, this, j)) return q("ajaxSubmit: submit aborted via beforeSubmit callback"), this;
        if (this.trigger("form-submit-validate", [s, this, j, i]), i.veto) return q("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this;
        var a = O.param(s, o);
        r && (a = a ? a + "&" + r : r), "GET" == j.type.toUpperCase() ? (j.url += (0 <= j.url.indexOf("?") ? "&" : "?") + a, j.data = null) : j.data = a;
        var l = [];
        if (j.resetForm && l.push(function () {
            D.resetForm()
        }), j.clearForm && l.push(function () {
            D.clearForm(j.includeHidden)
        }), !j.dataType && j.target) {
            var c = j.success || function () {
            };
            l.push(function (e) {
                var t = j.replaceTarget ? "replaceWith" : "html";
                O(j.target)[t](e).each(c, arguments)
            })
        } else j.success && l.push(j.success);
        if (j.success = function (e, t, n) {
            for (var i = j.context || this, o = 0, r = l.length; o < r; o++) l[o].apply(i, [e, t, n || D, D])
        }, j.error) {
            var u = j.error;
            j.error = function (e, t, n) {
                var i = j.context || this;
                u.apply(i, [e, t, n, D])
            }
        }
        if (j.complete) {
            var d = j.complete;
            j.complete = function (e, t) {
                var n = j.context || this;
                d.apply(n, [e, t, D])
            }
        }
        var p = 0 < O("input[type=file]:enabled", this).filter(function () {
                return "" !== O(this).val()
            }).length, f = "multipart/form-data", h = D.attr("enctype") == f || D.attr("encoding") == f,
            m = b.fileapi && b.formdata;
        q("fileAPI :" + m);
        var v, g = (p || h) && !m;
        !1 !== j.iframe && (j.iframe || g) ? j.closeKeepAlive ? O.get(j.closeKeepAlive, function () {
            v = e(s)
        }) : v = e(s) : v = (p || h) && m ? function (e) {
            for (var n = new FormData, t = 0; t < e.length; t++) n.append(e[t].name, e[t].value);
            if (j.extraData) {
                var i = function (e) {
                    var t, n, i = O.param(e, j.traditional).split("&"), o = i.length, r = [];
                    for (t = 0; t < o; t++) i[t] = i[t].replace(/\+/g, " "), n = i[t].split("="), r.push([decodeURIComponent(n[0]), decodeURIComponent(n[1])]);
                    return r
                }(j.extraData);
                for (t = 0; t < i.length; t++) i[t] && n.append(i[t][0], i[t][1])
            }
            j.data = null;
            var o = O.extend(!0, {}, O.ajaxSettings, j, {
                contentType: !1,
                processData: !1,
                cache: !1,
                type: L || "POST"
            });
            j.uploadProgress && (o.xhr = function () {
                var e = O.ajaxSettings.xhr();
                return e.upload && e.upload.addEventListener("progress", function (e) {
                    var t = 0, n = e.loaded || e.position, i = e.total;
                    e.lengthComputable && (t = Math.ceil(n / i * 100)), j.uploadProgress(e, n, i, t)
                }, !1), e
            }), o.data = null;
            var r = o.beforeSend;
            return o.beforeSend = function (e, t) {
                t.data = j.formData ? j.formData : n, r && r.call(this, e, t)
            }, O.ajax(o)
        }(s) : O.ajax(j), D.removeData("jqxhr").data("jqxhr", v);
        for (var y = 0; y < N.length; y++) N[y] = null;
        return this.trigger("form-submit-notify", [this, j]), this
    }, O.fn.ajaxForm = function (e) {
        if ((e = e || {}).delegation = e.delegation && O.isFunction(O.fn.on), e.delegation || 0 !== this.length) return e.delegation ? (O(document).off("submit.form-plugin", this.selector, n).off("click.form-plugin", this.selector, i).on("submit.form-plugin", this.selector, e, n).on("click.form-plugin", this.selector, e, i), this) : this.ajaxFormUnbind().bind("submit.form-plugin", e, n).bind("click.form-plugin", e, i);
        var t = {s: this.selector, c: this.context};
        return !O.isReady && t.s ? (q("DOM not ready, queuing ajaxForm"), O(function () {
            O(t.s, t.c).ajaxForm(e)
        })) : q("terminating; zero elements found by selector" + (O.isReady ? "" : " (DOM not ready)")), this
    }, O.fn.ajaxFormUnbind = function () {
        return this.unbind("submit.form-plugin click.form-plugin")
    }, O.fn.formToArray = function (e, t) {
        var n = [];
        if (0 === this.length) return n;
        var i, o, r, s, a, l, c, u, d = this[0], p = this.attr("id"), f = e ? d.getElementsByTagName("*") : d.elements;
        if (f && !/MSIE [678]/.test(navigator.userAgent) && (f = O(f).get()), p && ((i = O(':input[form="' + p + '"]').get()).length && (f = (f || []).concat(i))), !f || !f.length) return n;
        for (o = 0, c = f.length; o < c; o++) if ((s = (l = f[o]).name) && !l.disabled) if (e && d.clk && "image" == l.type) d.clk == l && (n.push({
            name: s,
            value: O(l).val(),
            type: l.type
        }), n.push({name: s + ".x", value: d.clk_x}, {
            name: s + ".y",
            value: d.clk_y
        })); else if ((a = O.fieldValue(l, !0)) && a.constructor == Array) for (t && t.push(l), r = 0, u = a.length; r < u; r++) n.push({
            name: s,
            value: a[r]
        }); else if (b.fileapi && "file" == l.type) {
            t && t.push(l);
            var h = l.files;
            if (h.length) for (r = 0; r < h.length; r++) n.push({
                name: s,
                value: h[r],
                type: l.type
            }); else n.push({name: s, value: "", type: l.type})
        } else null != a && (t && t.push(l), n.push({name: s, value: a, type: l.type, required: l.required}));
        if (!e && d.clk) {
            var m = O(d.clk), v = m[0];
            (s = v.name) && !v.disabled && "image" == v.type && (n.push({
                name: s,
                value: m.val()
            }), n.push({name: s + ".x", value: d.clk_x}, {name: s + ".y", value: d.clk_y}))
        }
        return n
    }, O.fn.formSerialize = function (e) {
        return O.param(this.formToArray(e))
    }, O.fn.fieldSerialize = function (o) {
        var r = [];
        return this.each(function () {
            var e = this.name;
            if (e) {
                var t = O.fieldValue(this, o);
                if (t && t.constructor == Array) for (var n = 0, i = t.length; n < i; n++) r.push({
                    name: e,
                    value: t[n]
                }); else null != t && r.push({name: this.name, value: t})
            }
        }), O.param(r)
    }, O.fn.fieldValue = function (e) {
        for (var t = [], n = 0, i = this.length; n < i; n++) {
            var o = this[n], r = O.fieldValue(o, e);
            null == r || r.constructor == Array && !r.length || (r.constructor == Array ? O.merge(t, r) : t.push(r))
        }
        return t
    }, O.fieldValue = function (e, t) {
        var n = e.name, i = e.type, o = e.tagName.toLowerCase();
        if (void 0 === t && (t = !0), t && (!n || e.disabled || "reset" == i || "button" == i || ("checkbox" == i || "radio" == i) && !e.checked || ("submit" == i || "image" == i) && e.form && e.form.clk != e || "select" == o && -1 == e.selectedIndex)) return null;
        if ("select" != o) return O(e).val();
        var r = e.selectedIndex;
        if (r < 0) return null;
        for (var s = [], a = e.options, l = "select-one" == i, c = l ? r + 1 : a.length, u = l ? r : 0; u < c; u++) {
            var d = a[u];
            if (d.selected) {
                var p = d.value;
                if (p || (p = d.attributes && d.attributes.value && !d.attributes.value.specified ? d.text : d.value), l) return p;
                s.push(p)
            }
        }
        return s
    }, O.fn.clearForm = function (e) {
        return this.each(function () {
            O("input,select,textarea", this).clearFields(e)
        })
    }, O.fn.clearFields = O.fn.clearInputs = function (n) {
        var i = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
        return this.each(function () {
            var e = this.type, t = this.tagName.toLowerCase();
            i.test(e) || "textarea" == t ? this.value = "" : "checkbox" == e || "radio" == e ? this.checked = !1 : "select" == t ? this.selectedIndex = -1 : "file" == e ? /MSIE/.test(navigator.userAgent) ? O(this).replaceWith(O(this).clone(!0)) : O(this).val("") : n && (!0 === n && /hidden/.test(e) || "string" == typeof n && O(this).is(n)) && (this.value = "")
        })
    }, O.fn.resetForm = function () {
        return this.each(function () {
            "function" != typeof this.reset && ("object" != _typeof(this.reset) || this.reset.nodeType) || this.reset()
        })
    }, O.fn.enable = function (e) {
        return void 0 === e && (e = !0), this.each(function () {
            this.disabled = !e
        })
    }, O.fn.selected = function (n) {
        return void 0 === n && (n = !0), this.each(function () {
            var e = this.type;
            if ("checkbox" == e || "radio" == e) this.checked = n; else if ("option" == this.tagName.toLowerCase()) {
                var t = O(this).parent("select");
                n && t[0] && "select-one" == t[0].type && t.find("option").selected(!1), this.selected = n
            }
        })
    }, O.fn.ajaxSubmit.debug = !1
}), function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function (c) {
    var o, s = window.Slick || {};
    (o = 0, s = function (e, t) {
        var n, i = this;
        i.defaults = {
            accessibility: !0,
            adaptiveHeight: !1,
            appendArrows: c(e),
            appendDots: c(e),
            arrows: !0,
            asNavFor: null,
            prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
            nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
            autoplay: !1,
            autoplaySpeed: 3e3,
            centerMode: !1,
            centerPadding: "50px",
            cssEase: "ease",
            customPaging: function (e, t) {
                return c('<button type="button" />').text(t + 1)
            },
            dots: !1,
            dotsClass: "slick-dots",
            draggable: !0,
            easing: "linear",
            edgeFriction: .35,
            fade: !1,
            focusOnSelect: !1,
            focusOnChange: !1,
            infinite: !0,
            initialSlide: 0,
            lazyLoad: "ondemand",
            mobileFirst: !1,
            pauseOnHover: !0,
            pauseOnFocus: !0,
            pauseOnDotsHover: !1,
            respondTo: "window",
            responsive: null,
            rows: 1,
            rtl: !1,
            slide: "",
            slidesPerRow: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 500,
            swipe: !0,
            swipeToSlide: !1,
            touchMove: !0,
            touchThreshold: 5,
            useCSS: !0,
            useTransform: !0,
            variableWidth: !1,
            vertical: !1,
            verticalSwiping: !1,
            waitForAnimate: !0,
            zIndex: 1e3
        }, i.initials = {
            animating: !1,
            dragging: !1,
            autoPlayTimer: null,
            currentDirection: 0,
            currentLeft: null,
            currentSlide: 0,
            direction: 1,
            $dots: null,
            listWidth: null,
            listHeight: null,
            loadIndex: 0,
            $nextArrow: null,
            $prevArrow: null,
            scrolling: !1,
            slideCount: null,
            slideWidth: null,
            $slideTrack: null,
            $slides: null,
            sliding: !1,
            slideOffset: 0,
            swipeLeft: null,
            swiping: !1,
            $list: null,
            touchObject: {},
            transformsEnabled: !1,
            unslicked: !1
        }, c.extend(i, i.initials), i.activeBreakpoint = null, i.animType = null, i.animProp = null, i.breakpoints = [], i.breakpointSettings = [], i.cssTransitions = !1, i.focussed = !1, i.interrupted = !1, i.hidden = "hidden", i.paused = !0, i.positionProp = null, i.respondTo = null, i.rowCount = 1, i.shouldClick = !0, i.$slider = c(e), i.$slidesCache = null, i.transformType = null, i.transitionType = null, i.visibilityChange = "visibilitychange", i.windowWidth = 0, i.windowTimer = null, n = c(e).data("slick") || {}, i.options = c.extend({}, i.defaults, t, n), i.currentSlide = i.options.initialSlide, i.originalSettings = i.options, void 0 !== document.mozHidden ? (i.hidden = "mozHidden", i.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (i.hidden = "webkitHidden", i.visibilityChange = "webkitvisibilitychange"), i.autoPlay = c.proxy(i.autoPlay, i), i.autoPlayClear = c.proxy(i.autoPlayClear, i), i.autoPlayIterator = c.proxy(i.autoPlayIterator, i), i.changeSlide = c.proxy(i.changeSlide, i), i.clickHandler = c.proxy(i.clickHandler, i), i.selectHandler = c.proxy(i.selectHandler, i), i.setPosition = c.proxy(i.setPosition, i), i.swipeHandler = c.proxy(i.swipeHandler, i), i.dragHandler = c.proxy(i.dragHandler, i), i.keyHandler = c.proxy(i.keyHandler, i), i.instanceUid = o++, i.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, i.registerBreakpoints(), i.init(!0)
    }).prototype.activateADA = function () {
        this.$slideTrack.find(".slick-active").attr({"aria-hidden": "false"}).find("a, input, button, select").attr({tabindex: "0"})
    }, s.prototype.addSlide = s.prototype.slickAdd = function (e, t, n) {
        var i = this;
        if ("boolean" == typeof t) n = t, t = null; else if (t < 0 || t >= i.slideCount) return !1;
        i.unload(), "number" == typeof t ? 0 === t && 0 === i.$slides.length ? c(e).appendTo(i.$slideTrack) : n ? c(e).insertBefore(i.$slides.eq(t)) : c(e).insertAfter(i.$slides.eq(t)) : !0 === n ? c(e).prependTo(i.$slideTrack) : c(e).appendTo(i.$slideTrack), i.$slides = i.$slideTrack.children(this.options.slide), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.append(i.$slides), i.$slides.each(function (e, t) {
            c(t).attr("data-slick-index", e)
        }), i.$slidesCache = i.$slides, i.reinit()
    }, s.prototype.animateHeight = function () {
        var e = this;
        if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.animate({height: t}, e.options.speed)
        }
    }, s.prototype.animateSlide = function (e, t) {
        var n = {}, i = this;
        i.animateHeight(), !0 === i.options.rtl && !1 === i.options.vertical && (e = -e), !1 === i.transformsEnabled ? !1 === i.options.vertical ? i.$slideTrack.animate({left: e}, i.options.speed, i.options.easing, t) : i.$slideTrack.animate({top: e}, i.options.speed, i.options.easing, t) : !1 === i.cssTransitions ? (!0 === i.options.rtl && (i.currentLeft = -i.currentLeft), c({animStart: i.currentLeft}).animate({animStart: e}, {
            duration: i.options.speed,
            easing: i.options.easing,
            step: function (e) {
                e = Math.ceil(e), !1 === i.options.vertical ? n[i.animType] = "translate(" + e + "px, 0px)" : n[i.animType] = "translate(0px," + e + "px)", i.$slideTrack.css(n)
            },
            complete: function () {
                t && t.call()
            }
        })) : (i.applyTransition(), e = Math.ceil(e), !1 === i.options.vertical ? n[i.animType] = "translate3d(" + e + "px, 0px, 0px)" : n[i.animType] = "translate3d(0px," + e + "px, 0px)", i.$slideTrack.css(n), t && setTimeout(function () {
            i.disableTransition(), t.call()
        }, i.options.speed))
    }, s.prototype.getNavTarget = function () {
        var e = this.options.asNavFor;
        return e && null !== e && (e = c(e).not(this.$slider)), e
    }, s.prototype.asNavFor = function (t) {
        var e = this.getNavTarget();
        null !== e && "object" == _typeof(e) && e.each(function () {
            var e = c(this).slick("getSlick");
            e.unslicked || e.slideHandler(t, !0)
        })
    }, s.prototype.applyTransition = function (e) {
        var t = this, n = {};
        !1 === t.options.fade ? n[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : n[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, !1 === t.options.fade ? t.$slideTrack.css(n) : t.$slides.eq(e).css(n)
    }, s.prototype.autoPlay = function () {
        var e = this;
        e.autoPlayClear(), e.slideCount > e.options.slidesToShow && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed))
    }, s.prototype.autoPlayClear = function () {
        this.autoPlayTimer && clearInterval(this.autoPlayTimer)
    }, s.prototype.autoPlayIterator = function () {
        var e = this, t = e.currentSlide + e.options.slidesToScroll;
        e.paused || e.interrupted || e.focussed || (!1 === e.options.infinite && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll, e.currentSlide - 1 == 0 && (e.direction = 1))), e.slideHandler(t))
    }, s.prototype.buildArrows = function () {
        var e = this;
        !0 === e.options.arrows && (e.$prevArrow = c(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = c(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }, s.prototype.buildDots = function () {
        var e, t, n = this;
        if (!0 === n.options.dots) {
            for (n.$slider.addClass("slick-dotted"), t = c("<ul />").addClass(n.options.dotsClass), e = 0; e <= n.getDotCount(); e += 1) t.append(c("<li />").append(n.options.customPaging.call(this, n, e)));
            n.$dots = t.appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active")
        }
    }, s.prototype.buildOut = function () {
        var e = this;
        e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
            c(t).attr("data-slick-index", e).data("originalStyling", c(t).attr("style") || "")
        }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? c('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), c("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable")
    }, s.prototype.buildRows = function () {
        var e, t, n, i, o, r, s, a = this;
        if (i = document.createDocumentFragment(), r = a.$slider.children(), 1 < a.options.rows) {
            for (s = a.options.slidesPerRow * a.options.rows, o = Math.ceil(r.length / s), e = 0; e < o; e++) {
                var l = document.createElement("div");
                for (t = 0; t < a.options.rows; t++) {
                    var c = document.createElement("div");
                    for (n = 0; n < a.options.slidesPerRow; n++) {
                        var u = e * s + (t * a.options.slidesPerRow + n);
                        r.get(u) && c.appendChild(r.get(u))
                    }
                    l.appendChild(c)
                }
                i.appendChild(l)
            }
            a.$slider.empty().append(i), a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }, s.prototype.checkResponsive = function (e, t) {
        var n, i, o, r = this, s = !1, a = r.$slider.width(), l = window.innerWidth || c(window).width();
        if ("window" === r.respondTo ? o = l : "slider" === r.respondTo ? o = a : "min" === r.respondTo && (o = Math.min(l, a)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
            for (n in i = null, r.breakpoints) r.breakpoints.hasOwnProperty(n) && (!1 === r.originalSettings.mobileFirst ? o < r.breakpoints[n] && (i = r.breakpoints[n]) : o > r.breakpoints[n] && (i = r.breakpoints[n]));
            null !== i ? null !== r.activeBreakpoint ? i === r.activeBreakpoint && !t || (r.activeBreakpoint = i, "unslick" === r.breakpointSettings[i] ? r.unslick(i) : (r.options = c.extend({}, r.originalSettings, r.breakpointSettings[i]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), s = i) : (r.activeBreakpoint = i, "unslick" === r.breakpointSettings[i] ? r.unslick(i) : (r.options = c.extend({}, r.originalSettings, r.breakpointSettings[i]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), s = i) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), s = i), e || !1 === s || r.$slider.trigger("breakpoint", [r, s])
        }
    }, s.prototype.changeSlide = function (e, t) {
        var n, i, o = this, r = c(e.currentTarget);
        switch (r.is("a") && e.preventDefault(), r.is("li") || (r = r.closest("li")), n = o.slideCount % o.options.slidesToScroll != 0 ? 0 : (o.slideCount - o.currentSlide) % o.options.slidesToScroll, e.data.message) {
            case"previous":
                i = 0 == n ? o.options.slidesToScroll : o.options.slidesToShow - n, o.slideCount > o.options.slidesToShow && o.slideHandler(o.currentSlide - i, !1, t);
                break;
            case"next":
                i = 0 == n ? o.options.slidesToScroll : n, o.slideCount > o.options.slidesToShow && o.slideHandler(o.currentSlide + i, !1, t);
                break;
            case"index":
                var s = 0 === e.data.index ? 0 : e.data.index || r.index() * o.options.slidesToScroll;
                o.slideHandler(o.checkNavigable(s), !1, t), r.children().trigger("focus");
                break;
            default:
                return
        }
    }, s.prototype.checkNavigable = function (e) {
        var t, n;
        if (n = 0, e > (t = this.getNavigableIndexes())[t.length - 1]) e = t[t.length - 1]; else for (var i in t) {
            if (e < t[i]) {
                e = n;
                break
            }
            n = t[i]
        }
        return e
    }, s.prototype.cleanUpEvents = function () {
        var e = this;
        e.options.dots && null !== e.$dots && (c("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", c.proxy(e.interrupt, e, !0)).off("mouseleave.slick", c.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), c(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().off("click.slick", e.selectHandler), c(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), c(window).off("resize.slick.slick-" + e.instanceUid, e.resize), c("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), c(window).off("load.slick.slick-" + e.instanceUid, e.setPosition)
    }, s.prototype.cleanUpSlideEvents = function () {
        var e = this;
        e.$list.off("mouseenter.slick", c.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", c.proxy(e.interrupt, e, !1))
    }, s.prototype.cleanUpRows = function () {
        var e;
        1 < this.options.rows && ((e = this.$slides.children().children()).removeAttr("style"), this.$slider.empty().append(e))
    }, s.prototype.clickHandler = function (e) {
        !1 === this.shouldClick && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
    }, s.prototype.destroy = function (e) {
        var t = this;
        t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), c(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
            c(this).attr("style", c(this).data("originalStyling"))
        }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t])
    }, s.prototype.disableTransition = function (e) {
        var t = {};
        t[this.transitionType] = "", !1 === this.options.fade ? this.$slideTrack.css(t) : this.$slides.eq(e).css(t)
    }, s.prototype.fadeSlide = function (e, t) {
        var n = this;
        !1 === n.cssTransitions ? (n.$slides.eq(e).css({zIndex: n.options.zIndex}), n.$slides.eq(e).animate({opacity: 1}, n.options.speed, n.options.easing, t)) : (n.applyTransition(e), n.$slides.eq(e).css({
            opacity: 1,
            zIndex: n.options.zIndex
        }), t && setTimeout(function () {
            n.disableTransition(e), t.call()
        }, n.options.speed))
    }, s.prototype.fadeSlideOut = function (e) {
        var t = this;
        !1 === t.cssTransitions ? t.$slides.eq(e).animate({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }))
    }, s.prototype.filterSlides = s.prototype.slickFilter = function (e) {
        var t = this;
        null !== e && (t.$slidesCache = t.$slides, t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit())
    }, s.prototype.focusHandler = function () {
        var n = this;
        n.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (e) {
            e.stopImmediatePropagation();
            var t = c(this);
            setTimeout(function () {
                n.options.pauseOnFocus && (n.focussed = t.is(":focus"), n.autoPlay())
            }, 0)
        })
    }, s.prototype.getCurrent = s.prototype.slickCurrentSlide = function () {
        return this.currentSlide
    }, s.prototype.getDotCount = function () {
        var e = this, t = 0, n = 0, i = 0;
        if (!0 === e.options.infinite) if (e.slideCount <= e.options.slidesToShow) ++i; else for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else if (!0 === e.options.centerMode) i = e.slideCount; else if (e.options.asNavFor) for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else i = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
        return i - 1
    }, s.prototype.getLeft = function (e) {
        var t, n, i, o, r = this, s = 0;
        return r.slideOffset = 0, n = r.$slides.first().outerHeight(!0), !0 === r.options.infinite ? (r.slideCount > r.options.slidesToShow && (r.slideOffset = r.slideWidth * r.options.slidesToShow * -1, o = -1, !0 === r.options.vertical && !0 === r.options.centerMode && (2 === r.options.slidesToShow ? o = -1.5 : 1 === r.options.slidesToShow && (o = -2)), s = n * r.options.slidesToShow * o), r.slideCount % r.options.slidesToScroll != 0 && e + r.options.slidesToScroll > r.slideCount && r.slideCount > r.options.slidesToShow && (s = e > r.slideCount ? (r.slideOffset = (r.options.slidesToShow - (e - r.slideCount)) * r.slideWidth * -1, (r.options.slidesToShow - (e - r.slideCount)) * n * -1) : (r.slideOffset = r.slideCount % r.options.slidesToScroll * r.slideWidth * -1, r.slideCount % r.options.slidesToScroll * n * -1))) : e + r.options.slidesToShow > r.slideCount && (r.slideOffset = (e + r.options.slidesToShow - r.slideCount) * r.slideWidth, s = (e + r.options.slidesToShow - r.slideCount) * n), r.slideCount <= r.options.slidesToShow && (s = r.slideOffset = 0), !0 === r.options.centerMode && r.slideCount <= r.options.slidesToShow ? r.slideOffset = r.slideWidth * Math.floor(r.options.slidesToShow) / 2 - r.slideWidth * r.slideCount / 2 : !0 === r.options.centerMode && !0 === r.options.infinite ? r.slideOffset += r.slideWidth * Math.floor(r.options.slidesToShow / 2) - r.slideWidth : !0 === r.options.centerMode && (r.slideOffset = 0, r.slideOffset += r.slideWidth * Math.floor(r.options.slidesToShow / 2)), t = !1 === r.options.vertical ? e * r.slideWidth * -1 + r.slideOffset : e * n * -1 + s, !0 === r.options.variableWidth && (i = r.slideCount <= r.options.slidesToShow || !1 === r.options.infinite ? r.$slideTrack.children(".slick-slide").eq(e) : r.$slideTrack.children(".slick-slide").eq(e + r.options.slidesToShow), t = !0 === r.options.rtl ? i[0] ? -1 * (r.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, !0 === r.options.centerMode && (i = r.slideCount <= r.options.slidesToShow || !1 === r.options.infinite ? r.$slideTrack.children(".slick-slide").eq(e) : r.$slideTrack.children(".slick-slide").eq(e + r.options.slidesToShow + 1), t = !0 === r.options.rtl ? i[0] ? -1 * (r.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, t += (r.$list.width() - i.outerWidth()) / 2)), t
    }, s.prototype.getOption = s.prototype.slickGetOption = function (e) {
        return this.options[e]
    }, s.prototype.getNavigableIndexes = function () {
        var e, t = this, n = 0, i = 0, o = [];
        for (e = !1 === t.options.infinite ? t.slideCount : (n = -1 * t.options.slidesToScroll, i = -1 * t.options.slidesToScroll, 2 * t.slideCount); n < e;) o.push(n), n = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        return o
    }, s.prototype.getSlick = function () {
        return this
    }, s.prototype.getSlideCount = function () {
        var n, i, o = this;
        return i = !0 === o.options.centerMode ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, !0 === o.options.swipeToSlide ? (o.$slideTrack.find(".slick-slide").each(function (e, t) {
            if (t.offsetLeft - i + c(t).outerWidth() / 2 > -1 * o.swipeLeft) return n = t, !1
        }), Math.abs(c(n).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll
    }, s.prototype.goTo = s.prototype.slickGoTo = function (e, t) {
        this.changeSlide({data: {message: "index", index: parseInt(e)}}, t)
    }, s.prototype.init = function (e) {
        var t = this;
        c(t.$slider).hasClass("slick-initialized") || (c(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay())
    }, s.prototype.initADA = function () {
        var n = this, i = Math.ceil(n.slideCount / n.options.slidesToShow),
            o = n.getNavigableIndexes().filter(function (e) {
                return 0 <= e && e < n.slideCount
            });
        n.$slides.add(n.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({tabindex: "-1"}), null !== n.$dots && (n.$slides.not(n.$slideTrack.find(".slick-cloned")).each(function (e) {
            var t = o.indexOf(e);
            c(this).attr({
                role: "tabpanel",
                id: "slick-slide" + n.instanceUid + e,
                tabindex: -1
            }), -1 !== t && c(this).attr({"aria-describedby": "slick-slide-control" + n.instanceUid + t})
        }), n.$dots.attr("role", "tablist").find("li").each(function (e) {
            var t = o[e];
            c(this).attr({role: "presentation"}), c(this).find("button").first().attr({
                role: "tab",
                id: "slick-slide-control" + n.instanceUid + e,
                "aria-controls": "slick-slide" + n.instanceUid + t,
                "aria-label": e + 1 + " of " + i,
                "aria-selected": null,
                tabindex: "-1"
            })
        }).eq(n.currentSlide).find("button").attr({"aria-selected": "true", tabindex: "0"}).end());
        for (var e = n.currentSlide, t = e + n.options.slidesToShow; e < t; e++) n.$slides.eq(e).attr("tabindex", 0);
        n.activateADA()
    }, s.prototype.initArrowEvents = function () {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.off("click.slick").on("click.slick", {message: "previous"}, e.changeSlide), e.$nextArrow.off("click.slick").on("click.slick", {message: "next"}, e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow.on("keydown.slick", e.keyHandler), e.$nextArrow.on("keydown.slick", e.keyHandler)))
    }, s.prototype.initDotEvents = function () {
        var e = this;
        !0 === e.options.dots && (c("li", e.$dots).on("click.slick", {message: "index"}, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && c("li", e.$dots).on("mouseenter.slick", c.proxy(e.interrupt, e, !0)).on("mouseleave.slick", c.proxy(e.interrupt, e, !1))
    }, s.prototype.initSlideEvents = function () {
        var e = this;
        e.options.pauseOnHover && (e.$list.on("mouseenter.slick", c.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", c.proxy(e.interrupt, e, !1)))
    }, s.prototype.initializeEvents = function () {
        var e = this;
        e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {action: "start"}, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {action: "move"}, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {action: "end"}, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {action: "end"}, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), c(document).on(e.visibilityChange, c.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().on("click.slick", e.selectHandler), c(window).on("orientationchange.slick.slick-" + e.instanceUid, c.proxy(e.orientationChange, e)), c(window).on("resize.slick.slick-" + e.instanceUid, c.proxy(e.resize, e)), c("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), c(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), c(e.setPosition)
    }, s.prototype.initUI = function () {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(), e.$nextArrow.show()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.show()
    }, s.prototype.keyHandler = function (e) {
        var t = this;
        e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === t.options.accessibility ? t.changeSlide({data: {message: !0 === t.options.rtl ? "next" : "previous"}}) : 39 === e.keyCode && !0 === t.options.accessibility && t.changeSlide({data: {message: !0 === t.options.rtl ? "previous" : "next"}}))
    }, s.prototype.lazyLoad = function () {
        function e(e) {
            c("img[data-lazy]", e).each(function () {
                var e = c(this), t = c(this).attr("data-lazy"), n = c(this).attr("data-srcset"),
                    i = c(this).attr("data-sizes") || r.$slider.attr("data-sizes"), o = document.createElement("img");
                o.onload = function () {
                    e.animate({opacity: 0}, 100, function () {
                        n && (e.attr("srcset", n), i && e.attr("sizes", i)), e.attr("src", t).animate({opacity: 1}, 200, function () {
                            e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
                        }), r.$slider.trigger("lazyLoaded", [r, e, t])
                    })
                }, o.onerror = function () {
                    e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), r.$slider.trigger("lazyLoadError", [r, e, t])
                }, o.src = t
            })
        }

        var t, n, i, r = this;
        if (!0 === r.options.centerMode ? i = !0 === r.options.infinite ? (n = r.currentSlide + (r.options.slidesToShow / 2 + 1)) + r.options.slidesToShow + 2 : (n = Math.max(0, r.currentSlide - (r.options.slidesToShow / 2 + 1)), r.options.slidesToShow / 2 + 1 + 2 + r.currentSlide) : (n = r.options.infinite ? r.options.slidesToShow + r.currentSlide : r.currentSlide, i = Math.ceil(n + r.options.slidesToShow), !0 === r.options.fade && (0 < n && n--, i <= r.slideCount && i++)), t = r.$slider.find(".slick-slide").slice(n, i), "anticipated" === r.options.lazyLoad) for (var o = n - 1, s = i, a = r.$slider.find(".slick-slide"), l = 0; l < r.options.slidesToScroll; l++) o < 0 && (o = r.slideCount - 1), t = (t = t.add(a.eq(o))).add(a.eq(s)), o--, s++;
        e(t), r.slideCount <= r.options.slidesToShow ? e(r.$slider.find(".slick-slide")) : r.currentSlide >= r.slideCount - r.options.slidesToShow ? e(r.$slider.find(".slick-cloned").slice(0, r.options.slidesToShow)) : 0 === r.currentSlide && e(r.$slider.find(".slick-cloned").slice(-1 * r.options.slidesToShow))
    }, s.prototype.loadSlider = function () {
        var e = this;
        e.setPosition(), e.$slideTrack.css({opacity: 1}), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad()
    }, s.prototype.next = s.prototype.slickNext = function () {
        this.changeSlide({data: {message: "next"}})
    }, s.prototype.orientationChange = function () {
        this.checkResponsive(), this.setPosition()
    }, s.prototype.pause = s.prototype.slickPause = function () {
        this.autoPlayClear(), this.paused = !0
    }, s.prototype.play = s.prototype.slickPlay = function () {
        var e = this;
        e.autoPlay(), e.options.autoplay = !0, e.paused = !1, e.focussed = !1, e.interrupted = !1
    }, s.prototype.postSlide = function (e) {
        var t = this;
        t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && (t.initADA(), t.options.focusOnChange && c(t.$slides.get(t.currentSlide)).attr("tabindex", 0).focus()))
    }, s.prototype.prev = s.prototype.slickPrev = function () {
        this.changeSlide({data: {message: "previous"}})
    }, s.prototype.preventDefault = function (e) {
        e.preventDefault()
    }, s.prototype.progressiveLazyLoad = function (e) {
        e = e || 1;
        var t, n, i, o, r, s = this, a = c("img[data-lazy]", s.$slider);
        a.length ? (t = a.first(), n = t.attr("data-lazy"), i = t.attr("data-srcset"), o = t.attr("data-sizes") || s.$slider.attr("data-sizes"), (r = document.createElement("img")).onload = function () {
            i && (t.attr("srcset", i), o && t.attr("sizes", o)), t.attr("src", n).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === s.options.adaptiveHeight && s.setPosition(), s.$slider.trigger("lazyLoaded", [s, t, n]), s.progressiveLazyLoad()
        }, r.onerror = function () {
            e < 3 ? setTimeout(function () {
                s.progressiveLazyLoad(e + 1)
            }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), s.$slider.trigger("lazyLoadError", [s, t, n]), s.progressiveLazyLoad())
        }, r.src = n) : s.$slider.trigger("allImagesLoaded", [s])
    }, s.prototype.refresh = function (e) {
        var t, n, i = this;
        n = i.slideCount - i.options.slidesToShow, !i.options.infinite && i.currentSlide > n && (i.currentSlide = n), i.slideCount <= i.options.slidesToShow && (i.currentSlide = 0), t = i.currentSlide, i.destroy(!0), c.extend(i, i.initials, {currentSlide: t}), i.init(), e || i.changeSlide({
            data: {
                message: "index",
                index: t
            }
        }, !1)
    }, s.prototype.registerBreakpoints = function () {
        var e, t, n, i = this, o = i.options.responsive || null;
        if ("array" === c.type(o) && o.length) {
            for (e in i.respondTo = i.options.respondTo || "window", o) if (n = i.breakpoints.length - 1, o.hasOwnProperty(e)) {
                for (t = o[e].breakpoint; 0 <= n;) i.breakpoints[n] && i.breakpoints[n] === t && i.breakpoints.splice(n, 1), n--;
                i.breakpoints.push(t), i.breakpointSettings[t] = o[e].settings
            }
            i.breakpoints.sort(function (e, t) {
                return i.options.mobileFirst ? e - t : t - e
            })
        }
    }, s.prototype.reinit = function () {
        var e = this;
        e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
    }, s.prototype.resize = function () {
        var e = this;
        c(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
            e.windowWidth = c(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
        }, 50))
    }, s.prototype.removeSlide = s.prototype.slickRemove = function (e, t, n) {
        var i = this;
        if (e = "boolean" == typeof e ? !0 === (t = e) ? 0 : i.slideCount - 1 : !0 === t ? --e : e, i.slideCount < 1 || e < 0 || e > i.slideCount - 1) return !1;
        i.unload(), !0 === n ? i.$slideTrack.children().remove() : i.$slideTrack.children(this.options.slide).eq(e).remove(), i.$slides = i.$slideTrack.children(this.options.slide), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.append(i.$slides), i.$slidesCache = i.$slides, i.reinit()
    }, s.prototype.setCSS = function (e) {
        var t, n, i = this, o = {};
        !0 === i.options.rtl && (e = -e), t = "left" == i.positionProp ? Math.ceil(e) + "px" : "0px", n = "top" == i.positionProp ? Math.ceil(e) + "px" : "0px", o[i.positionProp] = e, !1 === i.transformsEnabled || (!(o = {}) === i.cssTransitions ? o[i.animType] = "translate(" + t + ", " + n + ")" : o[i.animType] = "translate3d(" + t + ", " + n + ", 0px)"), i.$slideTrack.css(o)
    }, s.prototype.setDimensions = function () {
        var e = this;
        !1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({padding: "0px " + e.options.centerPadding}) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), !0 === e.options.centerMode && e.$list.css({padding: e.options.centerPadding + " 0px"})), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
        var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
        !1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
    }, s.prototype.setFade = function () {
        var n, i = this;
        i.$slides.each(function (e, t) {
            n = i.slideWidth * e * -1, !0 === i.options.rtl ? c(t).css({
                position: "relative",
                right: n,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            }) : c(t).css({position: "relative", left: n, top: 0, zIndex: i.options.zIndex - 2, opacity: 0})
        }), i.$slides.eq(i.currentSlide).css({zIndex: i.options.zIndex - 1, opacity: 1})
    }, s.prototype.setHeight = function () {
        var e = this;
        if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.css("height", t)
        }
    }, s.prototype.setOption = s.prototype.slickSetOption = function () {
        var e, t, n, i, o, r = this, s = !1;
        if ("object" === c.type(arguments[0]) ? (n = arguments[0], s = arguments[1], o = "multiple") : "string" === c.type(arguments[0]) && (n = arguments[0], i = arguments[1], s = arguments[2], "responsive" === arguments[0] && "array" === c.type(arguments[1]) ? o = "responsive" : void 0 !== arguments[1] && (o = "single")), "single" === o) r.options[n] = i; else if ("multiple" === o) c.each(n, function (e, t) {
            r.options[e] = t
        }); else if ("responsive" === o) for (t in i) if ("array" !== c.type(r.options.responsive)) r.options.responsive = [i[t]]; else {
            for (e = r.options.responsive.length - 1; 0 <= e;) r.options.responsive[e].breakpoint === i[t].breakpoint && r.options.responsive.splice(e, 1), e--;
            r.options.responsive.push(i[t])
        }
        s && (r.unload(), r.reinit())
    }, s.prototype.setPosition = function () {
        var e = this;
        e.setDimensions(), e.setHeight(), !1 === e.options.fade ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(), e.$slider.trigger("setPosition", [e])
    }, s.prototype.setProps = function () {
        var e = this, t = document.body.style;
        e.positionProp = !0 === e.options.vertical ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || !0 === e.options.useCSS && (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && !1 !== e.animType
    }, s.prototype.setSlideClasses = function (e) {
        var t, n, i, o, r = this;
        if (n = r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), r.$slides.eq(e).addClass("slick-current"), !0 === r.options.centerMode) {
            var s = r.options.slidesToShow % 2 == 0 ? 1 : 0;
            t = Math.floor(r.options.slidesToShow / 2), !0 === r.options.infinite && (t <= e && e <= r.slideCount - 1 - t ? r.$slides.slice(e - t + s, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (i = r.options.slidesToShow + e, n.slice(i - t + 1 + s, i + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? n.eq(n.length - 1 - r.options.slidesToShow).addClass("slick-center") : e === r.slideCount - 1 && n.eq(r.options.slidesToShow).addClass("slick-center")), r.$slides.eq(e).addClass("slick-center")
        } else 0 <= e && e <= r.slideCount - r.options.slidesToShow ? r.$slides.slice(e, e + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : n.length <= r.options.slidesToShow ? n.addClass("slick-active").attr("aria-hidden", "false") : (o = r.slideCount % r.options.slidesToShow, i = !0 === r.options.infinite ? r.options.slidesToShow + e : e, r.options.slidesToShow == r.options.slidesToScroll && r.slideCount - e < r.options.slidesToShow ? n.slice(i - (r.options.slidesToShow - o), i + o).addClass("slick-active").attr("aria-hidden", "false") : n.slice(i, i + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
        "ondemand" !== r.options.lazyLoad && "anticipated" !== r.options.lazyLoad || r.lazyLoad()
    }, s.prototype.setupInfinite = function () {
        var e, t, n, i = this;
        if (!0 === i.options.fade && (i.options.centerMode = !1), !0 === i.options.infinite && !1 === i.options.fade && (t = null, i.slideCount > i.options.slidesToShow)) {
            for (n = !0 === i.options.centerMode ? i.options.slidesToShow + 1 : i.options.slidesToShow, e = i.slideCount; e > i.slideCount - n; e -= 1) t = e - 1, c(i.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - i.slideCount).prependTo(i.$slideTrack).addClass("slick-cloned");
            for (e = 0; e < n + i.slideCount; e += 1) t = e, c(i.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + i.slideCount).appendTo(i.$slideTrack).addClass("slick-cloned");
            i.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
                c(this).attr("id", "")
            })
        }
    }, s.prototype.interrupt = function (e) {
        e || this.autoPlay(), this.interrupted = e
    }, s.prototype.selectHandler = function (e) {
        var t = c(e.target).is(".slick-slide") ? c(e.target) : c(e.target).parents(".slick-slide"),
            n = parseInt(t.attr("data-slick-index"));
        n || (n = 0), this.slideCount <= this.options.slidesToShow ? this.slideHandler(n, !1, !0) : this.slideHandler(n)
    }, s.prototype.slideHandler = function (e, t, n) {
        var i, o, r, s, a, l = null, c = this;
        if (t = t || !1, !(!0 === c.animating && !0 === c.options.waitForAnimate || !0 === c.options.fade && c.currentSlide === e)) if (!1 === t && c.asNavFor(e), i = e, l = c.getLeft(i), s = c.getLeft(c.currentSlide), c.currentLeft = null === c.swipeLeft ? s : c.swipeLeft, !1 === c.options.infinite && !1 === c.options.centerMode && (e < 0 || e > c.getDotCount() * c.options.slidesToScroll)) !1 === c.options.fade && (i = c.currentSlide, !0 !== n ? c.animateSlide(s, function () {
            c.postSlide(i)
        }) : c.postSlide(i)); else if (!1 === c.options.infinite && !0 === c.options.centerMode && (e < 0 || e > c.slideCount - c.options.slidesToScroll)) !1 === c.options.fade && (i = c.currentSlide, !0 !== n ? c.animateSlide(s, function () {
            c.postSlide(i)
        }) : c.postSlide(i)); else {
            if (c.options.autoplay && clearInterval(c.autoPlayTimer), o = i < 0 ? c.slideCount % c.options.slidesToScroll != 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + i : i >= c.slideCount ? c.slideCount % c.options.slidesToScroll != 0 ? 0 : i - c.slideCount : i, c.animating = !0, c.$slider.trigger("beforeChange", [c, c.currentSlide, o]), r = c.currentSlide, c.currentSlide = o, c.setSlideClasses(c.currentSlide), c.options.asNavFor && (a = (a = c.getNavTarget()).slick("getSlick")).slideCount <= a.options.slidesToShow && a.setSlideClasses(c.currentSlide), c.updateDots(), c.updateArrows(), !0 === c.options.fade) return !0 !== n ? (c.fadeSlideOut(r), c.fadeSlide(o, function () {
                c.postSlide(o)
            })) : c.postSlide(o), void c.animateHeight();
            !0 !== n ? c.animateSlide(l, function () {
                c.postSlide(o)
            }) : c.postSlide(o)
        }
    }, s.prototype.startLoad = function () {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
    }, s.prototype.swipeDirection = function () {
        var e, t, n, i, o = this;
        return e = o.touchObject.startX - o.touchObject.curX, t = o.touchObject.startY - o.touchObject.curY, n = Math.atan2(t, e), (i = Math.round(180 * n / Math.PI)) < 0 && (i = 360 - Math.abs(i)), i <= 45 && 0 <= i ? !1 === o.options.rtl ? "left" : "right" : i <= 360 && 315 <= i ? !1 === o.options.rtl ? "left" : "right" : 135 <= i && i <= 225 ? !1 === o.options.rtl ? "right" : "left" : !0 === o.options.verticalSwiping ? 35 <= i && i <= 135 ? "down" : "up" : "vertical"
    }, s.prototype.swipeEnd = function (e) {
        var t, n, i = this;
        if (i.dragging = !1, i.swiping = !1, i.scrolling) return i.scrolling = !1;
        if (i.interrupted = !1, i.shouldClick = !(10 < i.touchObject.swipeLength), void 0 === i.touchObject.curX) return !1;
        if (!0 === i.touchObject.edgeHit && i.$slider.trigger("edge", [i, i.swipeDirection()]), i.touchObject.swipeLength >= i.touchObject.minSwipe) {
            switch (n = i.swipeDirection()) {
                case"left":
                case"down":
                    t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide + i.getSlideCount()) : i.currentSlide + i.getSlideCount(), i.currentDirection = 0;
                    break;
                case"right":
                case"up":
                    t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide - i.getSlideCount()) : i.currentSlide - i.getSlideCount(), i.currentDirection = 1
            }
            "vertical" != n && (i.slideHandler(t), i.touchObject = {}, i.$slider.trigger("swipe", [i, n]))
        } else i.touchObject.startX !== i.touchObject.curX && (i.slideHandler(i.currentSlide), i.touchObject = {})
    }, s.prototype.swipeHandler = function (e) {
        var t = this;
        if (!(!1 === t.options.swipe || "ontouchend" in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
            case"start":
                t.swipeStart(e);
                break;
            case"move":
                t.swipeMove(e);
                break;
            case"end":
                t.swipeEnd(e)
        }
    }, s.prototype.swipeMove = function (e) {
        var t, n, i, o, r, s, a = this;
        return r = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!a.dragging || a.scrolling || r && 1 !== r.length) && (t = a.getLeft(a.currentSlide), a.touchObject.curX = void 0 !== r ? r[0].pageX : e.clientX, a.touchObject.curY = void 0 !== r ? r[0].pageY : e.clientY, a.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(a.touchObject.curX - a.touchObject.startX, 2))), s = Math.round(Math.sqrt(Math.pow(a.touchObject.curY - a.touchObject.startY, 2))), !a.options.verticalSwiping && !a.swiping && 4 < s ? !(a.scrolling = !0) : (!0 === a.options.verticalSwiping && (a.touchObject.swipeLength = s), n = a.swipeDirection(), void 0 !== e.originalEvent && 4 < a.touchObject.swipeLength && (a.swiping = !0, e.preventDefault()), o = (!1 === a.options.rtl ? 1 : -1) * (a.touchObject.curX > a.touchObject.startX ? 1 : -1), !0 === a.options.verticalSwiping && (o = a.touchObject.curY > a.touchObject.startY ? 1 : -1), i = a.touchObject.swipeLength, (a.touchObject.edgeHit = !1) === a.options.infinite && (0 === a.currentSlide && "right" === n || a.currentSlide >= a.getDotCount() && "left" === n) && (i = a.touchObject.swipeLength * a.options.edgeFriction, a.touchObject.edgeHit = !0), !1 === a.options.vertical ? a.swipeLeft = t + i * o : a.swipeLeft = t + i * (a.$list.height() / a.listWidth) * o, !0 === a.options.verticalSwiping && (a.swipeLeft = t + i * o), !0 !== a.options.fade && !1 !== a.options.touchMove && (!0 === a.animating ? (a.swipeLeft = null, !1) : void a.setCSS(a.swipeLeft))))
    }, s.prototype.swipeStart = function (e) {
        var t, n = this;
        if (n.interrupted = !0, 1 !== n.touchObject.fingerCount || n.slideCount <= n.options.slidesToShow) return !(n.touchObject = {});
        void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), n.touchObject.startX = n.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, n.touchObject.startY = n.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, n.dragging = !0
    }, s.prototype.unfilterSlides = s.prototype.slickUnfilter = function () {
        var e = this;
        null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.appendTo(e.$slideTrack), e.reinit())
    }, s.prototype.unload = function () {
        var e = this;
        c(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }, s.prototype.unslick = function (e) {
        this.$slider.trigger("unslick", [this, e]), this.destroy()
    }, s.prototype.updateArrows = function () {
        var e = this;
        Math.floor(e.options.slidesToShow / 2), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }, s.prototype.updateDots = function () {
        var e = this;
        null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").end(), e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active"))
    }, s.prototype.visibility = function () {
        this.options.autoplay && (document[this.hidden] ? this.interrupted = !0 : this.interrupted = !1)
    }, c.fn.slick = function () {
        var e, t, n = this, i = arguments[0], o = Array.prototype.slice.call(arguments, 1), r = n.length;
        for (e = 0; e < r; e++) if ("object" == _typeof(i) || void 0 === i ? n[e].slick = new s(n[e], i) : t = n[e].slick[i].apply(n[e].slick, o), void 0 !== t) return t;
        return n
    }
}), function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == ("undefined" == typeof exports ? "undefined" : _typeof(exports)) ? require("jquery") : jQuery)
}(function (S) {
    var i, e = navigator.userAgent, C = /iphone/i.test(e), o = /chrome/i.test(e), A = /android/i.test(e);
    S.mask = {
        definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
        autoclear: !0,
        dataName: "rawMaskFn",
        placeholder: "_"
    }, S.fn.extend({
        caret: function (e, t) {
            var n;
            if (0 !== this.length && !this.is(":hidden")) return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function () {
                this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && ((n = this.createTextRange()).collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
            })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
                begin: e,
                end: t
            })
        }, unmask: function () {
            return this.trigger("unmask")
        }, mask: function (t, g) {
            var n, y, b, w, x, T, k;
            if (!t && 0 < this.length) {
                var e = S(this[0]).data(S.mask.dataName);
                return e ? e() : void 0
            }
            return g = S.extend({
                autoclear: S.mask.autoclear,
                placeholder: S.mask.placeholder,
                completed: null
            }, g), n = S.mask.definitions, y = [], b = T = t.length, w = null, S.each(t.split(""), function (e, t) {
                "?" == t ? (T--, b = e) : n[t] ? (y.push(new RegExp(n[t])), null === w && (w = y.length - 1), e < b && (x = y.length - 1)) : y.push(null)
            }), this.trigger("unmask").each(function () {
                function s() {
                    if (g.completed) {
                        for (var e = w; e <= x; e++) if (y[e] && h[e] === a(e)) return;
                        g.completed.call(f)
                    }
                }

                function a(e) {
                    return g.placeholder.charAt(e < g.placeholder.length ? e : 0)
                }

                function l(e) {
                    for (; ++e < T && !y[e];) ;
                    return e
                }

                function c(e, t) {
                    var n, i;
                    if (!(e < 0)) {
                        for (n = e, i = l(t); n < T; n++) if (y[n]) {
                            if (!(i < T && y[n].test(h[i]))) break;
                            h[n] = h[i], h[i] = a(i), i = l(i)
                        }
                        d(), f.caret(Math.max(w, e))
                    }
                }

                function r() {
                    p(), f.val() != v && f.change()
                }

                function u(e, t) {
                    var n;
                    for (n = e; n < t && n < T; n++) y[n] && (h[n] = a(n))
                }

                function d() {
                    f.val(h.join(""))
                }

                function p(e) {
                    var t, n, i, o = f.val(), r = -1;
                    for (i = t = 0; t < T; t++) if (y[t]) {
                        for (h[t] = a(t); i++ < o.length;) if (n = o.charAt(i - 1), y[t].test(n)) {
                            h[t] = n, r = t;
                            break
                        }
                        if (i > o.length) {
                            u(t + 1, T);
                            break
                        }
                    } else h[t] === o.charAt(i) && i++, t < b && (r = t);
                    return e ? d() : r + 1 < b ? g.autoclear || h.join("") === m ? (f.val() && f.val(""), u(0, T)) : d() : (d(), f.val(f.val().substring(0, r + 1))), b ? t : w
                }

                var f = S(this), h = S.map(t.split(""), function (e, t) {
                    return "?" != e ? n[e] ? a(t) : e : void 0
                }), m = h.join(""), v = f.val();
                f.data(S.mask.dataName, function () {
                    return S.map(h, function (e, t) {
                        return y[t] && e != a(t) ? e : null
                    }).join("")
                }), f.one("unmask", function () {
                    f.off(".mask").removeData(S.mask.dataName)
                }).on("focus.mask", function () {
                    var e;
                    f.prop("readonly") || (clearTimeout(i), v = f.val(), e = p(), i = setTimeout(function () {
                        f.get(0) === document.activeElement && (d(), e == t.replace("?", "").length ? f.caret(0, e) : f.caret(e))
                    }, 10))
                }).on("blur.mask", r).on("keydown.mask", function (e) {
                    if (!f.prop("readonly")) {
                        var t, n, i, o = e.which || e.keyCode;
                        k = f.val(), 8 === o || 46 === o || C && 127 === o ? (n = (t = f.caret()).begin, (i = t.end) - n == 0 && (n = 46 !== o ? function (e) {
                            for (; 0 <= --e && !y[e];) ;
                            return e
                        }(n) : i = l(n - 1), i = 46 === o ? l(i) : i), u(n, i), c(n, i - 1), e.preventDefault()) : 13 === o ? r.call(this, e) : 27 === o && (f.val(v), f.caret(0, p()), e.preventDefault())
                    }
                }).on("keypress.mask", function (e) {
                    if (!f.prop("readonly")) {
                        var t, n, i, o = e.which || e.keyCode, r = f.caret();
                        if (!(e.ctrlKey || e.altKey || e.metaKey || o < 32) && o && 13 !== o) {
                            if (r.end - r.begin != 0 && (u(r.begin, r.end), c(r.begin, r.end - 1)), (t = l(r.begin - 1)) < T && (n = String.fromCharCode(o), y[t].test(n))) {
                                if (function (e) {
                                    var t, n, i, o;
                                    for (n = a(t = e); t < T; t++) if (y[t]) {
                                        if (i = l(t), o = h[t], h[t] = n, !(i < T && y[i].test(o))) break;
                                        n = o
                                    }
                                }(t), h[t] = n, d(), i = l(t), A) {
                                    setTimeout(function () {
                                        S.proxy(S.fn.caret, f, i)()
                                    }, 0)
                                } else f.caret(i);
                                r.begin <= x && s()
                            }
                            e.preventDefault()
                        }
                    }
                }).on("input.mask paste.mask", function () {
                    f.prop("readonly") || setTimeout(function () {
                        var e = p(!0);
                        f.caret(e), s()
                    }, 0)
                }), o && A && f.off("input.mask").on("input.mask", function () {
                    var e = f.val(), t = f.caret();
                    if (k && k.length && k.length > e.length) {
                        for (p(!0); 0 < t.begin && !y[t.begin - 1];) t.begin--;
                        if (0 === t.begin) for (; t.begin < w && !y[t.begin];) t.begin++;
                        f.caret(t.begin, t.begin)
                    } else {
                        for (p(!0); t.begin < T && !y[t.begin];) t.begin++;
                        f.caret(t.begin, t.begin)
                    }
                    s()
                }), p()
            })
        }
    })
}), Element.prototype.matches || (Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector), Element.prototype.closest || (Element.prototype.closest = function (e) {
    var t = this;
    if (!document.documentElement.contains(t)) return null;
    do {
        if (t.matches(e)) return t;
        t = t.parentElement || t.parentNode
    } while (null !== t && 1 === t.nodeType);
    return null
}), Element.prototype.toggleClass = function (e) {
    this.classList.contains(e) ? this.classList.remove(e) : this.classList.add(e)
};
var pageScrollState = function () {
    function e() {
        document.body.parentElement.classList.contains("fixed") || (n = window.scrollY, document.body.parentNode.classList.add("fixed"), document.body.scrollTop = n)
    }

    function t() {
        document.body.parentElement.classList.contains("fixed") && (document.body.parentNode.classList.remove("fixed"), window.scrollTo(0, n))
    }

    var n = 0;
    return {
        fix: e, unfix: t, toggle: function () {
            document.body.parentNode.classList.contains("fixed") ? t() : e()
        }, set: function (e) {
            window.scrollTo(0, e)
        }
    }
}(), Convert = {
    nodeListToArray: function (e) {
        return Array.prototype.slice.call(e)
    }, toIntOrZero: function (e) {
        return e = parseInt(e), isNaN(e) ? 0 : e
    }, toFloatOrZero: function (e) {
        return e = parseFloat(e), isNaN(e) ? 0 : e
    }
}, querySelectorAsArray = function (e) {
    return Array.prototype.slice.call(document.querySelectorAll(e))
}, isDesktop = function () {
    return window.innerWidth > MOBILE_MAX_WIDTH
}, isMobile = function () {
    return !isDesktop()
}, on = function (e, t, n) {
    "string" == typeof e ? e = querySelectorAsArray(e) : e instanceof NodeList && (e = Array.prototype.slice.call(e)), e.forEach(function (e) {
        e.addEventListener(t, n)
    })
}, emailRegExp = function () {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
}, emailValid = function (e) {
    return emailRegExp().test(e)
}, phoneInputFilterRegExp = function () {
    return /[\d+() -]/
}, isInputFilled = function (e) {
    return "" !== e.value
}, isInputValid = function (e) {
    return "email" === e.type.toLowerCase() ? "" !== e.value && emailRegExp().test(e.value) : "" !== e.value && e.validity.valid
}, scrollToElement = function (e) {
    var i = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 1e3,
        t = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : 0;
    "string" == typeof e && (e = document.querySelector(e));
    for (var o = e.offsetTop; "body" !== e.tagName.toLowerCase();) e = e.parentElement, o += e.offsetTop;
    o += t;
    var r, s = window.scrollY, a = performance.now(), l = (o - s) / i;
    requestAnimationFrame(function e(t) {
        var n = t - a;
        i < n && (n = i), r = n === i ? o : s + l * n, window.scrollTo(0, r), n < i && requestAnimationFrame(e)
    })
}, animateElements = function (e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : ANIMATION_STEP, n = 0;
    Array.prototype.forEach.call(e, function (e) {
        e.classList.contains("animate-with-prev-element") || ++n, setTimeout(function () {
            e.classList.remove("unanimated"), e.classList.add("animated")
        }, t * n)
    })
}, isClickFromKeyboard = function (e) {
    if (!e.type || "click" !== e.type.toLowerCase()) throw new Error("Wrong event type!");
    return 0 === e.clientX && 0 === e.clientY
};

function setupPhoneInputFilter() {
    on('input[type="tel"]', "keypress", function (e) {
        var t = e.target;
        if (17 < t.value.length) return t.value = t.value.substr(0, 16), !1;
        if ("INPUT" === t.tagName.toUpperCase()) {
            var n = e.which;
            if (n < 32 || e.ctrlKey || e.altKey) return !0;
            var i = String.fromCharCode(n);
            if (!phoneInputFilterRegExp().test(i)) return e.preventDefault(), !1
        }
        return !0
    })
}

var scrollEventListenerThirdArgument = !1;
!function () {
    try {
        var e = Object.defineProperty({}, "passive", {
            get: function () {
                scrollEventListenerThirdArgument = {passive: !0}
            }
        });
        window.addEventListener("test", null, e)
    } catch (e) {
    }
}();
var MOBILE_MAX_WIDTH = 768, ANIMATION_STEP = 150, sliderArrows = {
    prevArrow: '<div class="slider-arrow slider-arrow-prev"></div>',
    nextArrow: '<div class="slider-arrow slider-arrow-next"></div>'
}, showFormMessage = function (e, t) {
    var n = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "",
        i = t.querySelector(".message-".concat(e));
    i.classList.add("active"), setTimeout(function () {
        i.classList.remove("active"), n && closeModal(n)
    }, 5e3)
}, clearForm = function (e) {
    return Array.prototype.forEach.call(e.querySelectorAll('input:not([type="hidden"]), textarea'), function (e) {
        return e.value = ""
    })
};
document.addEventListener("DOMContentLoaded", function () {
    function e() {
        var e = window.pageYOffset || document.documentElement.scrollTop;
        0 < e && !t.classList.contains("scrolled") && t.classList.add("scrolled"), 0 === e && t.classList.contains("scrolled") && t.classList.remove("scrolled")
    }

    var t = document.querySelector(".header-container");
    e(), window.addEventListener("scroll", e, scrollEventListenerThirdArgument);
    var i = document.querySelector(".header-container");
    on("#menu-mobile-modal .menu li:not(.no-js) > a, .header-container .menu li:not(.no-js) > a", "click", function (e) {
        e.preventDefault();
        var t = this.closest(".modal");
        t && closeModal(t);
        var n = -i.getBoundingClientRect().height;
        scrollToElement(this.getAttribute("href"), 1e3, n)
    }), $("input[name=tel]").mask("+38 (099) 999-99-99"), on("input, textarea", "input", function (e) {
        e.target.classList.contains("invalid") && e.target.classList.remove("invalid")
    }), querySelectorAsArray('input[class="agree"]').forEach(function (e) {
        e.checked = !1
    }), $(".contact-form").ajaxForm({
        data: {ajax: !0}, beforeSerialize: function (e) {
            var n = !0;
            return e.find("[required]").each(function (e, t) {
                isInputValid(t) || (n = !1, t.classList.add("invalid"))
            }), n
        }, success: function (e, t, n, i) {
            e = JSON.parse(e), showFormMessage(e.status, i[0]), clearForm(i[0])
        }, error: function () {
            for (var e = arguments.length, t = new Array(e), n = 0; n < e; n++) t[n] = arguments[n];
            showFormMessage("error", t[3][0]), clearForm(t[3][0])
        }
    }), window.addEventListener("load", function () {
        var e;
        e = t.classList.contains("scrolled") ? (e = (e = (e = querySelectorAsArray(".header-container .phone-container")).concat(querySelectorAsArray(".header-container .social-network"))).concat(querySelectorAsArray(".header-container .menu-button svg"))).concat(querySelectorAsArray(".header-container .menu .menu-item, .header-container .lang-switcher")) : querySelectorAsArray(".header-container .phone-container, .header-container .menu .menu-item, .header-container .social-network, .header-container .lang-switcher, .header-container .menu-button svg"), animateElements(e, 80), window.onPageLoadAnimationCallBack && setTimeout(window.onPageLoadAnimationCallBack, 80 * e.length)
    });
    var o = querySelectorAsArray(".animate-on-scroll");

    function n() {
        if (0 !== o.length) {
            var n = .8 * window.innerHeight;
            o.forEach(function (e) {
                if (!e.classList.contains("animated")) {
                    var t = e.getBoundingClientRect();
                    t.top < 0 && t.top < -t.height || (e.dataset.startAnimationOffsetCoefficient && (n = window.innerHeight * e.dataset.startAnimationOffsetCoefficient), t.top - n < 0 && setTimeout(function () {
                        e.classList.add("animated"), e.classList.contains("animate-by-elements") && animateElements(e.querySelectorAll(".block-animation-element"), e.dataset.animationDuration)
                    }, Convert.toIntOrZero(e.dataset.animationDelay)))
                }
            }), o = o.filter(function (e) {
                return !e.classList.contains("animated")
            })
        }
    }

    n(), window.addEventListener("scroll", n, scrollEventListenerThirdArgument), window.addEventListener("resize", n);
    var r = $("#request-section").offset().top - $(window).height();
    $(window).scroll(function () {
        var e = $(this).scrollTop();
        r < e ? $(".header-container-request").addClass("active") : $(".header-container-request").removeClass("active")
    }), $(document).on("click", ".header-container-request", function (e) {
        e.preventDefault();
        var t = $(this).attr("href"), n = $(t).offset().top, i = $(this);
        $("body,html").animate({scrollTop: n}, 1500, function () {
            i.addClass("active")
        })
    })
});
var modalIdsArray = ["menu-mobile-modal", "calc-form-modal"], openModal = function (e) {
    e = "string" == typeof e ? document.getElementById(e) : e, pageScrollState.fix(), e.dataset.bgId && document.getElementById(e.dataset.bgId).classList.add("active"), e.classList.add("active"), -1 !== modalIdsArray.indexOf(e.id) && onMenuOpen(e.id)
}, closeModal = function (e) {
    (e = "string" == typeof e ? document.getElementById(e) : e).dataset.bgId && document.getElementById(e.dataset.bgId).classList.remove("active"), e.classList.remove("active"), pageScrollState.unfix(), -1 !== modalIdsArray.indexOf(e.id) && onMenuClose(e.id)
};
querySelectorAsArray(".modal").forEach(function (e) {
    e.dataset.bgId ? document.getElementById(e.dataset.bgId).addEventListener("click", function () {
        closeModal(e)
    }) : e.addEventListener("click", function () {
        closeModal(e)
    });
    var t = e.querySelector(".close-btn");
    t && t.addEventListener("click", function () {
        closeModal(e)
    });
    var n = e.querySelector(".window");
    n && n.addEventListener("click", function (e) {
        e.stopPropagation()
    })
}), querySelectorAsArray(".modal-opener").forEach(function (e) {
    e.addEventListener("click", function () {
        openModal(e.dataset.modalId)
    })
});
var menuAnimationElements = querySelectorAsArray("#menu-mobile-modal .lang-switcher, #menu-mobile-modal .close-btn, #menu-mobile-modal .menu .menu-item, #menu-mobile-modal .managers-container .manager"),
    calcAnimationElements = querySelectorAsArray("#calc-form-modal .lang-switcher, #calc-form-modal .close-btn, #calc-form-modal .animate-to-top"),
    onMenuOpen = function (e) {
        switch (e) {
            case"menu-mobile-modal":
                animateElements(menuAnimationElements, 80);
                break;
            case"calc-form-modal":
                animateElements(calcAnimationElements, 60);
                break;
            default:
                return
        }
    }, onMenuClose = function (e) {
        switch (e) {
            case"menu-mobile-modal":
                menuAnimationElements.forEach(function (e) {
                    return e.classList.remove("animated")
                });
                break;
            case"calc-form-modal":
                calcAnimationElements.forEach(function (e) {
                    return e.classList.remove("animated")
                });
                break;
            default:
                return
        }
    };