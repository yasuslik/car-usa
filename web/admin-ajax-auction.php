<?php
require __DIR__ . '/../vendor/autoload.php';

use HtmlGenerator\HtmlTag;


$postData = $_POST;
$auctionData = $postData['data']['COPART'];
$resultHtml = '        <!-- Title -->
        <div class="auction__result-title">
            <span>Результат</span>
            <h2>' . $auctionData['header'] . ', Vin:' . $auctionData['keyword'] . '</h2>
        </div>
        <!-- End Title -->

        <!-- Row -->
        <div class="row">

                        <!-- Row > Right -->
            <div class="col-xs-12 col-lg-12">

                <!-- Table -->
                <div class="auction__result-table columnResizeTable">

                    <!-- Title -->
                    <div class="auction__result-table--title">
                        <img
                                src="/wp-content/themes/carbuy/images/auction/table-title-infoauction.png"
                                alt="Информация по акциону"
                                title="Информация по акциону"
                        >
                        <span>Информация по акциону</span>
                    </div>
                    <!-- End Title -->
                    <!-- table -->
                    <table cellpadding="0" cellspacing="0" style="margin-bottom: 40px">
                        <thead>
                        <tr>
                            <th width="50%">Final Bind:</th>
                            <th width="50%"><span></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Auction:</td>
                            <td><strong>' . $auctionData['service'] . '</strong></td>
                        </tr>
                        <tr>
                            <td>Auction Date And Time:</td>
                            <td>' . $auctionData['auctionDateAndTime'] . '</td>
                        </tr>

                        <tr>
                            <td>Lot number:</td>
                            <td>' . $auctionData['lotNumber'] . '</td>
                        </tr>
                        <tr>
                            <td>Seller:</td>
                            <td>' . $auctionData['seller'] . '</td>
                        </tr>
                        <tr>
                            <td>SellingBranch:</td>
                            <td>' . $auctionData['sellingBranch'] . '</td>
                        </tr>
                        <tr>
                            <td>Title:</td>
                            <td>' . $auctionData['title'] . '</td>
                        </tr>
                        <tr>
                            <td>Actual Cash Value:</td>
                            <td>' . $auctionData['actualCashValue'] . '</td>
                        </tr>
                        <tr>
                            <td>Status:</td>
                            <td>' . $auctionData['status'] . '</td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- end table -->

                    <!-- Title -->
                    <div class="auction__result-table--title">
                        <img
                                src="/wp-content/themes/carbuy/images/auction/table-title-infocar.png"
                                alt="Информация по машине"
                                title="Информация по машине"
                        >
                        <span>Информация по машине</span>
                    </div>
                    <!-- End Title -->
                    <!-- table -->
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td width="50%">BodyStyle:</td>
                            <td width="50%">' . $auctionData['bodyStyle'] . '</td>
                        </tr>
                        <tr>
                            <td>DriveLineType:</td>
                            <td>' . $auctionData['driveLineType'] . '</td>
                        </tr>
                        <tr>
                            <td>Fuel:</td>
                            <td>' . $auctionData['fuel'] . '</td>
                        </tr>
                        <tr>
                            <td>Engine:</td>
                            <td>' . $auctionData['engine'] . '</td>
                        </tr>
                        <tr>
                            <td>Cylinders:</td>
                            <td>' . $auctionData['cylinders'] . '</td>
                        </tr>
                        <tr>
                            <td>Transmission:</td>
                            <td>' . $auctionData['transmission'] . '</td>
                        </tr>
                        <tr>
                            <td>Odometer:</td>
                            <td>' . $auctionData['odometer'] . '</td>
                        </tr>
                        <tr>
                            <td>Loss:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Primary Damage:</td>
                            <td>' . $auctionData['primaryDamage'] . '</td>
                        </tr>
                        <tr>
                            <td>Secondary Damage:</td>
                            <td>' . $auctionData['secondaryDamage'] . '</td>
                        </tr>
                        <tr>
                            <td>StartCode:</td>
                            <td>' . $auctionData['startCode'] . '</td>
                        </tr>
                        <tr>
                            <td>EstimatedRepairCost:</td>
                            <td>' . $auctionData['estimatedRepairCost'] . '</td>
                        </tr>
                        <tr>
                            <td>Airbags:</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- end table -->

                    
                </div>
                <!-- End Table -->

            </div>
            <!-- End Row > Right -->
        </div>
        <!-- End Row -->
    ';

echo $resultHtml;
