<?php

return '<!-- Header -->
<div class="auction__result-trakheader">

    <!-- title -->
    <div class="auction__result-trakheader--title">
        <span>Номер контейнера</span>
        TRHU4318149
    </div>
    <!-- end title -->

    <!-- Link Search -->
    <a href="https://carbuy.com.ua/tracking/">
        <img
                src="https://carbuy.com.ua/wp-content/themes/carbuy/images/auction/auction-icon-search.png"
                alt=""
                title=""
        >
        <span>Новый поиск</span>
    </a>
    <!-- End Link Search -->

    <!-- Link Printer -->
    <a href="">
        <img
                src="https://carbuy.com.ua/wp-content/themes/carbuy/images/auction/auction-icon-printer.png"
                alt=""
                title=""
        >
        <span>Распечатать</span>
    </a>
    <!-- End Link Printer -->

</div>
<!-- End Header -->

<!-- Table -->
<div class="auction__result-traktable">
    <!-- table -->
    <table>
        <thead>
        <tr>
            <th>Container Line</th>
            <th>Container Type</th>
            <th>Destination Point</th>
            <th>Estimated Date</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td data-caption="Container Line">Maersk</td>
            <td data-caption="Container Type">40 Dry</td>
            <td data-caption="Destination Point">Pivdennyi</td>
            <td data-caption="Estimated Date">Feb 21 2021</td>
        </tr>
        </tbody>
    </table>
    <!-- end table -->
</div>
<!-- End Table -->

<!-- List -->
<ul class="auction__result-traklist">
    <li>
        <!-- Status -->
        <div>
            <span>Status</span>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <span>Location</span>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <span>Date</span>
        </div>
        <!-- End Date -->
    </li>
    <li>
        <!-- Status -->
        <div class="auction__result-traklist--status">
            <p>
                GATE-OUT-EMPTY <br>Vessel: MSC MARIA ELENA <br>Voyage: 102E </p>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <p>United States<br> Newark<br> Apm Terminal - Berth 88 E425</p>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <p>Jan 15 2021</p>
        </div>
        <!-- End Date -->
    </li>
    <li>
        <!-- Status -->
        <div class="auction__result-traklist--status">
            <p>
                GATE-IN <br>Vessel: MSC MARIA ELENA <br>Voyage: 102E </p>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <p>United States<br> Newark<br> Apm Terminal - Berth 88 E425</p>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <p>Jan 18 2021</p>
        </div>
        <!-- End Date -->
    </li>
    <li>
        <!-- Status -->
        <div class="auction__result-traklist--status">
            <p>
                LOAD <br>Vessel: MSC MARIA ELENA <br>Voyage: 102E </p>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <p>United States<br> Newark<br> Apm Terminal - Berth 88 E425</p>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <p>Jan 22 2021</p>
        </div>
        <!-- End Date -->
    </li>
    <li>
        <!-- Status -->
        <div class="auction__result-traklist--status">
            <p>
                LOAD <br>Vessel: LICA MAERSK <br>Voyage: 103E </p>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <p>Morocco<br> Port Tangier Mediterranee<br> Port Tangier Mediterranee</p>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <p>Feb 09 2021</p>
        </div>
        <!-- End Date -->
    </li>
    <li>
        <!-- Status -->
        <div class="auction__result-traklist--status">
            <p>
                DISCHARG <br>Vessel: MSC MARIA ELENA <br>Voyage: 102E </p>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <p>Morocco<br> Port Tangier Mediterranee<br> Port Tangier Mediterranee</p>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <p>Feb 14 2021</p>
        </div>
        <!-- End Date -->
    </li>
    <li>
        <!-- Status -->
        <div class="auction__result-traklist--status">
            <p>
                DISCHARG <br>Vessel: LICA MAERSK <br>Voyage: 103E </p>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <p>Ukraine<br> Pivdennyi<br> TIS</p>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <p>Feb 21 2021</p>
        </div>
        <!-- End Date -->
    </li>
    <li>
        <!-- Status -->
        <div class="auction__result-traklist--status">
            <p>
                GATE-OUT <br>Vessel: LICA MAERSK <br>Voyage: 103E </p>
        </div>
        <!-- End Status -->
        <!-- Location -->
        <div>
            <p>Ukraine<br> Pivdennyi<br> TIS</p>
        </div>
        <!-- End Location -->
        <!-- Date -->
        <div>
            <p>Feb 21 2021</p>
        </div>
        <!-- End Date -->
    </li>
</ul>
<!-- End List -->

    ';