<?php
require __DIR__ . '/../vendor/autoload.php';

use HtmlGenerator\HtmlTag;


$postData = $_POST;
$mainData = $postData['data'];
$tracks = $mainData['tracks'];
$lastElement = array_key_last($tracks);
$timestampNormalise = substr($tracks[$lastElement]['date'], 0, -3);
$estimatedDate = date("M d Y", $timestampNormalise);

$title = HtmlTag::createElement('div');
$title->addClass('auction__result-trakheader--title');
$title->addElement('span')->text('Номер контейнера');
$title->text($mainData['trackingNumber']);

$linkSearch = HtmlTag::createElement('a');
$linkSearch->set('href', './tracking.html');
$linkSearch->addElement('img')
    ->set('src', 'https://carbuy.com.ua/wp-content/themes/carbuy/images/auction/auction-icon-search.png')
    ->set('alt', '')
    ->set('title', '');
$linkSearch->addElement('span')->text('Новый поиск');

$linkPrinter = HtmlTag::createElement('a');
$linkPrinter->set('href', '');
$linkPrinter->addElement('img')
    ->set('src', 'https://carbuy.com.ua/wp-content/themes/carbuy/images/auction/auction-icon-printer.png')
    ->set('alt', '')
    ->set('title', '');
$linkPrinter->addElement('span')->text('Распечатать');

$header = '<div class="auction__result-trakheader">' . $title . $linkSearch . $linkPrinter . '</div>';

$tableHead = HtmlTag::createElement('table');
$tableHead->addElement('thead')
    ->addElement('tr')
    ->addElement('th')->text('Container Line')->getParent()
    ->addElement('th')->text('Container Type')->getParent()
    ->addElement('th')->text('Destination Point')->getParent()
    ->addElement('th')->text('Estimated Date')->getParent();
$tableHead->addElement('tbody')
    ->addElement('tr')
    ->addElement('td')->set('data-caption', 'Container Line')->text($mainData['service'])->getParent()
    ->addElement('td')->set('data-caption', 'Container Type')->text($mainData['containerType'])->getParent()
    ->addElement('td')->set('data-caption', 'Destination Point')->text($mainData['destination'])->getParent()
    ->addElement('td')->set('data-caption', 'Estimated Date')->text($estimatedDate);

$table = '<div class="auction__result-traktable">'. $tableHead . '</div>';

$list = '<ul class="auction__result-traklist">';
$listHead = HtmlTag::createElement('li');
$listHead->addElement('div')
    ->addElement('span')->text('Status');
$listHead->addElement('div')
    ->addElement('span')->text('Location');
$listHead->addElement('div')
    ->addElement('span')->text('Date');
$list .= $listHead;


foreach ($tracks as $track) {
    $location = str_replace(',', '<br>', $track['location']);
    $timestampNormalise = substr($track['date'], 0, -3);
    $estimatedDate = date("M d Y", $timestampNormalise);

    $listElement = HtmlTag::createElement('li');
    $listElement->addElement('div')->addClass('auction__result-traklist--status')
        ->addElement('p')->text($track['description']. " <br>Vessel: " . $track['vessel'] . " <br>Voyage: " . $track['vesselType']);
    $listElement->addElement('div')
        ->addElement('p')->text($location);
    $listElement->addElement('div')
        ->addElement('p')->text($estimatedDate);


    $list.= $listElement;
}
$list.= '</ul>';

$result = $header . $table . $list;
echo $result;
